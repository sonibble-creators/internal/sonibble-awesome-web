# Docker file configuration
# by sonibble.com

# contain docker image builder
# to make the awesome space of image

# dependecies stage
# allow to build the dependency for 
# this app like node_modules
# the main goal is to build the node_modules
FROM node:16-alpine as dependencies
WORKDIR /app
COPY package*.json ./
RUN npm install

# build stage
# we going to build the app
# define the actual source will be serve 
# to the server
# will create build and public/build folder
FROM node:16-alpine as build
WORKDIR /app
COPY . .
COPY --from=dependencies /app/node_modules ./node_modules
RUN npm run build

# runner stage
# copy all of the source 
# like build folder, modules, and setting files
FROM node:16-alpine as runner

# define some env variables
ENV NODE_ENV=production
ENV PORT=3000

# work directory
WORKDIR /app

# Copy all of the source data 
# we need (just needed) to reduce the image size
COPY --from=build /app/build ./build
COPY --from=build /app/public/build ./public/build
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/package*.json ./

# expose the application based on the port
EXPOSE ${PORT}

# run the application
# with specify port
CMD PORT=${PORT} npm run start