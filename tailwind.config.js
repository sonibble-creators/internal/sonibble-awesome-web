module.exports = {
  prefix: "sn-",
  content: ["./app/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      sans: ["DM Sans", "sans-serif"],
    },
    extend: {
      fontSize: {
        "display-1": [
          "5.375rem",
          {
            lineHeight: "1.5em",
          },
        ],
        "display-2": [
          "3.5rem",
          {
            lineHeight: "1.5em",
          },
        ],
      },
    },
  },
  plugins: [require("tailwindcss-text-fill-stroke")()],
};
