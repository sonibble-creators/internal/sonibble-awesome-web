import { Icon } from "@iconify/react";
import { Link } from "remix";

/**
 * # FooterComponent
 * simple footer for general use
 *
 *
 * @return JSX.Element
 */

export default function FooterComponent(): JSX.Element {
  return (
    <footer
      className="footer sn-bg-black sn-w-full sn-pt-20 sn-pb-12 md:sn-pt-36 md:sn-pb-16"
      id="footer"
      data-jelly-scroll
      data-cursor="before:sn-bg-white"
    >
      <div className="footer-wrapper sn-container sn-mx-auto flex sn-flex-col">
        <div
          className="headline sn-flex sn-flex-col sn-items-center sn-space-y-1"
          data-cursor="cursor-lg before:sn-bg-white"
        >
          <span className="sn-text-3xl md:sn-text-display-2 sn-font-medium sn-text-gray-100 text-center">
            Stay connected
          </span>
          <span className="sn-text-3xl md:sn-text-display-2 sn-font-medium sn-text-gray-100 text-center">
            with creators
          </span>
        </div>
        <div className="headline-description sn-mt-10 sn-flex sn-justify-center">
          <span className="sn-text-base sn-text-center md:sn-text-xl sn-text-gray-200 sn-font-sans sn-max-w-2xl">
            Stay connected with creative creators for your next project, see
            what are build, learn, and update. Always bring the creative idea
            into your dream project, service and more
          </span>
        </div>

        {/* actions
          including the email, action, socials and more
        */}

        <div className="social action sn-flex sn-flex-col sn-mt-44 sn-justify-center">
          <a
            href="mailto:creative.sonibble@gmail.com"
            target="_blank"
            className="email-actions sn-font-medium sn-text-gray-100 sn-text-base :sn-text-lg sn-flex sn-justify-center"
            data-cursor="cursor-opaque cursor-inverse"
          >
            creative.sonibble@gmail.com
          </a>
          <ul className="social-menus sn-flex sn-gap-8 sn-justify-center sn-mt-12">
            <li
              className="sn-h-14 sn-w-14 sn-rounded-2xl sn-bg-white sn-text-black sn-flex sn-justify-center sn-items-center"
              data-magnetic
            >
              <a
                href="https://instagram.com/sonibble"
                target="_blank"
                className=""
              >
                <Icon
                  icon="ant-design:instagram-outlined"
                  className="sn-w-7 sn-h-7"
                />
              </a>
            </li>

            <li
              className="sn-h-14 sn-w-14 sn-rounded-2xl sn-bg-white sn-text-black sn-flex sn-justify-center sn-items-center"
              data-magnetic
            >
              <a
                href="https:gitlab.com/sonibble-creators"
                target="_blank"
                className=""
              >
                <Icon
                  icon="ant-design:gitlab-outlined"
                  className="sn-w-7 sn-h-7"
                />
              </a>
            </li>
            <li
              className="sn-h-14 sn-w-14 sn-rounded-2xl sn-bg-white sn-text-black sn-flex sn-justify-center sn-items-center"
              data-magnetic
            >
              <a
                href="https://dribble.com/sonibble"
                target="_blank"
                className=""
              >
                <Icon
                  icon="ant-design:dribbble-outlined"
                  className="sn-w-7 sn-h-7"
                />
              </a>
            </li>
          </ul>
        </div>

        {/* bottom content
          including the credit and other resource link
        */}

        <div className="bottom-content-footer sn-flex sn-mt-20 md:sn-mt-72 sn-px-4 md:sn-px-16">
          <span className="sn-text-gray-200">
            Copyright 2022 Sonibble - Alright Reserved
          </span>
          <div className="sn-grow"></div>
          <ul className="menu-buttons sn-contents sn-list-none">
            <li className="sn-text-gray-200">
              <Link to="/privacy">Privacy Policy</Link>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
}
