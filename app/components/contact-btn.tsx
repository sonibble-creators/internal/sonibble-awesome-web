import { Icon } from "@iconify/react";
import { Link } from "remix";

/**
 * # ContactButtonComponent
 *
 * contact button to show in every page,
 * so make easy the user to reach the contact pages
 * @returns
 */
export default function ContactButtonComponent() {
  return (
    <Link
      to="/contact"
      className="sn-h-16 sn-w-16 sn-bg-black sn-rounded-full sn-fixed sn-bottom-4 sn-right-4 md:sn-bottom-14 md:sn-right-14 sn-z-[19] sn-group"
      data-magnetic
    >
      <div className="sn-relative sn-overflow-hidden sn-flex sn-h-full sn-w-full sn-rounded-full sn-justify-center sn-items-center">
        <span className="sn-absolute sn-inset-0 sn-translate-y-20 group-hover:sn-translate-y-0 sn-bg-white sn-transition-all sn-duration-700 sn-rounded-full"></span>
        <Icon
          icon="bi:chat-fill"
          className="sn-h-5 sn-w-5 sn-text-white sn-relative group-hover:sn-text-black"
        />
      </div>
    </Link>
  );
}
