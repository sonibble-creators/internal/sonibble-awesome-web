import { useState } from "react";
import { gsap } from "gsap";
import { Link } from "remix";

/**
 * # MenuComponent
 *
 * showing the menu button and enable user to see the main menu with very satisfied
 * contain the menu link, and button
 * @return JSX.Element
 */
export default function MenuComponent(): JSX.Element {
  // define the state
  // enable to toggle the button of this menu
  // the purppose is to change the icons, and so on
  const [isMenuActive, setMenuActive] = useState(false);

  // the on click button handler
  // we will doing manipulation here
  const onMenuClicked: React.MouseEventHandler = () => {
    // we need to check if the menu active or not
    if (isMenuActive) {
      // we need to diactive it
      // hide the whole content
      gsap.to(".sn-menu-content", {
        duration: 1.2,
        translateX: "-150%",
      });

      // we going to hide the background
      setTimeout(() => {
        gsap.utils.toArray(".sn-menu-bg").forEach((bg: any, index: number) => {
          gsap.to(bg, {
            duration: 1.2,
            translateY: "-200%",
            delay: index * -0.1,
          });
        });
      }, 1200);
    } else {
      // we need to show the menu
      // we need to show and animate the background
      gsap.utils.toArray(".sn-menu-bg").forEach((bg: any, index: number) => {
        gsap.to(bg, {
          duration: 1.2,
          translateY: 0,
          delay: index * 0.3,
        });
      });

      // now show the whole content
      gsap.to(".sn-menu-content", {
        duration: 1.2,
        translateX: 0,
        delay: 1.3,
        ease: "elastic.out(1, 0.7)",
      });
    }
    setMenuActive(!isMenuActive);
  };

  return (
    <div className="sn-menu">
      {/* the button */}
      <button
        className="sn-menu-btn sn-h-16 sn-w-16 sn-bg-white sn-rounded-full sn-fixed sn-right-2 sn-top-2 md:sn-top-6 md:sn-right-14 sn-z-40 sn-group"
        data-magnetic
        onClick={onMenuClicked}
      >
        <div className="sn-relative sn-h-full sn-w-full sn-overflow-hidden sn-flex sn-justify-center sn-items-center sn-flex-col sn-rounded-full">
          {/* the background */}
          <span className="sn-absolute sn-inset-0 sn-translate-y-20 group-hover:sn-translate-y-0 sn-rounded-full sn-bg-black sn-transition-all sn-duration-700"></span>
          {/* the top of burger menu */}
          <span
            className={
              isMenuActive
                ? "sn-w-7 sn-h-0.5 sn-rounded-full sn-bg-black group-hover:sn-bg-white sn-relative sn-transition-all sn-duration-700 sn-will-change-transform sn-transform sn-rotate-45"
                : "sn-w-7 sn-h-0.5 sn-rounded-full sn-bg-black group-hover:sn-bg-white sn-relative sn-transition-all sn-duration-700"
            }
          ></span>

          {/* the second line of burger menu */}
          <span
            className={
              isMenuActive
                ? "sn-w-7 sn-h-0.5 sn-rounded-full sn-bg-black group-hover:sn-bg-white sn-relative sn-transition-all sn-duration-700 sn-will-change-transform sn-transform sn-rotate-[-45deg]"
                : "sn-w-7 sn-h-0.5 sn-rounded-full sn-bg-black group-hover:sn-bg-white sn-relative sn-transition-all sn-duration-700 sn-mt-1"
            }
          ></span>
        </div>
      </button>

      {/* menu section */}
      {/* background stack */}
      {/* the backgorund will be animated with simple waves */}
      {/* there are some background will be animated */}
      {/* but the main background absolutly is white */}
      <div className="sn-menu-background sn-relative">
        {/* place other background here */}
        {/* you can customize the animation background here */}
        <div className="sn-menu-bg sn-fixed sn-bg-black sn-inset-0 sn-z-[33] sn-translate-y-[-200%] sn-rounded-b-full sn-scale-[2]"></div>
        <div className="sn-menu-bg sn-fixed sn-bg-white sn-inset-0 sn-z-[33] sn-translate-y-[-200%] sn-rounded-b-full sn-scale-[2]"></div>
      </div>

      {/* menu link */}
      {/* contain many element of each link */}
      <div className="wrapper sn-fixed sn-h-full sn-w-full sn-inset-0 sn-z-[33] sn-translate-x-[-150%] sn-menu-content">
        <div className="sn-flex sn-flex-col md:sn-flex-row sn-px-10 sn-py-8 sn-container sn-mx-auto md:sn-py-28 md:sn-px-28 sn-gap-16 md:sn-gap-80 sn-overflow-y-auto md:sn-overflow-y-hidden sn-h-full">
          {/* menu here */}
          {/* contain the main menu an pages for the wesbsite */}
          <div className="sn-flex sn-flex-col sn-gap-10 md:sn-gap-20">
            <span className="sn-text-lg sn-text-gray-600">Menu</span>

            <ul className="sn-list-none sn-flex sn-flex-col sn-gap-8">
              <li
                className="sn-font-medium sn-text-black sn-text-4xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <Link to="">Home</Link>
              </li>
              <li
                className="sn-font-medium sn-text-black sn-text-4xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <Link to="/about">About</Link>
              </li>
              <li
                className="sn-font-medium sn-text-black sn-text-4xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <Link to="/contact">Contact</Link>
              </li>
              <li
                className="sn-font-medium sn-text-black sn-text-4xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <Link to="/product">Product</Link>
              </li>
              <li
                className="sn-font-medium sn-text-black sn-text-4xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <Link to="/service">Services</Link>
              </li>
              <li
                className="sn-font-medium sn-text-black sn-text-4xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <Link to="/work">Works</Link>
              </li>
            </ul>
          </div>

          {/* SOCIAL MEDIA */}
          {/* list all of the social media, links, email, and other platform */}
          <div className="sn-flex sn-flex-col sn-gap-10 md:sn-gap-20">
            <span className="sn-text-lg sn-text-gray-600">Stay Connected</span>

            <ul className="sn-list-none sn-flex sn-flex-col sn-gap-8">
              <li
                className="sn-text-black sn-text-xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <a href="mailto:creative.sonibble@gmail.com" target="_blank">
                  Email
                </a>
              </li>
              <li
                className="sn-text-black sn-text-xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <a href="https://instagram.com/sonibble" target="_blank">
                  Instagram
                </a>
              </li>
              <li
                className="sn-text-black sn-text-xl"
                data-magnetic
                data-cursor="cursor-opaque cursor-inverse"
                onClick={onMenuClicked}
              >
                <a href="https://gitlab.com/sonibble-creators" target="_blank">
                  Gitlab
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
