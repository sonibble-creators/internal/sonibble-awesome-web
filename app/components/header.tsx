import { Link } from "remix";
import logo from "~/images/favicon.png";

/**
 * # HeaderComponent
 *
 * simple header for general use
 * contain many element including the logo
 * @returns JSX.Element
 */
export default function HeaderComponent() {
  return (
    <header className="header simple-header" id="header">
      <div className="header-wrapper sn-container sn-flex md:sn-mx-auto sn-py-6 sn-items-center sn-mx-4">
        <div className="brand" data-magnetic>
          <Link to="/" className="sn-text-2xl sn-font-medium">
            <img src={logo} alt="Sonibble" className="sn-w-14 sn-h-14" />
          </Link>
        </div>
        <div className="sn-grow"></div>
        <div className="menus sn-flex sn-list-none sn-mr-20 md:sn-mr-14">
          <li className="sn-text-base sn-font-medium" data-cursor="-opaque">
            Menu
          </li>
        </div>
      </div>
    </header>
  );
}
