import { gsap } from "gsap";

/**
 * # animateHeroHeaderHeadline
 *
 * use to animate the hero header
 * will show mask and circle masking for your header
 *
 * TODO: add some element to your html like `.hero-headline-animated`, `.hero-headline-text`, `.hero-headline-shape-item`
 */
export function runHeroHeaderAnimation() {
  // get the element
  // espoecial to animate the
  const headingElement = document.querySelector(".hero-headline-animated");
  const container = document.querySelector(".hero-headline-text");
  const shape = document.querySelectorAll(".hero-headline-shape-item");

  headingElement?.addEventListener("mousemove", (e) => {
    // Get cursor position relative to box with text
    if (container != null) {
      const rect = container.getBoundingClientRect();
      const y = e.clientY - rect.top;
      const x = e.clientX - rect.left;
      // Animate shapes
      gsap.to(shape, {
        x: x,
        y: y,
        duration: (i) => 1.4 - i * 0.2,
        delay: (i) => i * 0.1,
        stagger: 0.02,
        ease: "expo.out",
      });
    }
  });
}
