import gsap from "gsap";
import Scrollbar from "smooth-scrollbar";
import "intersection-observer";

/**
 * # Jelly Effect
 * Enable jelly efect when scrolling
 * give more smooth and awesome effect in each section
 * of element
 */
export class JellyEffect {
  constructor(options) {
    this.jellys = [];
    this.els = document.querySelectorAll("[data-jelly-scroll]");
    for (let i = 0; i < this.els.length; i++) {
      const el = this.els[i];
      const opts = Object.assign(
        {},
        options,
        el.dataset.sonibbleJelly ? JSON.parse(el.dataset.sonibbleJelly) : {}
      );
      const jelly = new Jelly(el, opts);
      this.jellys.push(jelly);
    }
  }

  pause(state) {
    for (let i = 0; i < this.jellys.length; i++) this.jellys[i].pause(state);
  }

  destroy() {
    for (let i = 0; i < this.jellys.length; i++) this.jellys[i].destroy();
  }
}

/**
 * # Jelly Class
 * jelly cnfiguration when running animation
 */
export class Jelly {
  constructor(el, options) {
    this.el = el;
    this.paused = false;
    this.options = Object.assign(
      {},
      {
        intensity: 0.15,
        speed: 0.8,
        min: -5,
        max: 5,
        scrollPos: () => window.scrollY,
      },
      options
    );
    this.init();
  }

  getScrollPos() {
    return this.options.scrollPos();
  }

  pause(state = true) {
    this.paused = state;
  }

  init() {
    this.observer = new IntersectionObserver((entries) => {
      if (entries[0].isIntersecting) {
        this.startLoop();
      } else {
        this.stopLoop();
      }
    });
    this.observer.observe(this.el);
  }

  startLoop() {
    this.y = this.getScrollPos();
    this.loop();
  }

  stopLoop() {
    cancelAnimationFrame(this.frame);

    if (!this.paused) {
      gsap.set(this.el, { skewY: 0 });
    }
  }

  loop() {
    const y = this.getScrollPos();
    const diff = y - this.y;
    const skew = Math.min(
      Math.max(this.options.min, diff * this.options.intensity),
      this.options.max
    );

    if (!this.paused) {
      gsap.set(this.el, { skewY: skew, force3D: true });
      this.animationPaused = false;
    } else {
      if (!this.animationPaused) {
        gsap.to(this.el, {
          skewY: 0,
          force3D: true,
          duration: this.options.speed,
        });
        this.animationPaused = true;
      }
    }

    this.y = y;
    this.frame = requestAnimationFrame(this.loop.bind(this));
  }

  destroy() {
    this.observer.disconnect();
    this.pause(false);
    this.stopLoop();
  }
}

/**
 * # SoftScrollPlugin
 * Scroll plugin
 * custom scroll plugin, to enable soft effect
 */
class SoftScrollPlugin extends Scrollbar.ScrollbarPlugin {
  transformDelta(delta, fromEvent) {
    const dirX = delta.x > 0 ? 1 : -1;
    const dirY = delta.y > 0 ? 1 : -1;

    if (dirX === this.lockX || dirY === this.lockY) {
      return { x: 0, y: 0 };
    } else {
      this.lockX = null;
      this.lockY = null;
    }

    return delta;
  }

  onRender(Data2d) {
    const { x, y } = Data2d;

    // Up
    if (y < 0 && !this.lockY && Math.abs(y) >= this.scrollbar.scrollTop) {
      this.scrollbar.setMomentum(0, -this.scrollbar.scrollTop);
      this.lockY = -1;
    }

    // Left
    if (x < 0 && !this.lockX && Math.abs(x) >= this.scrollbar.scrollLeft) {
      this.scrollbar.setMomentum(-this.scrollbar.scrollLeft, 0);
      this.lockX = -1;
    }

    // Right
    if (
      x > 0 &&
      !this.lockX &&
      Math.abs(x) >= this.scrollbar.limit.x - this.scrollbar.scrollLeft
    ) {
      this.scrollbar.setMomentum(
        this.scrollbar.limit.x - this.scrollbar.scrollLeft,
        0
      );
      this.lockX = 1;
    }

    // Down
    if (
      y > 0 &&
      !this.lockY &&
      Math.abs(y) >= this.scrollbar.limit.y - this.scrollbar.scrollTop
    ) {
      this.scrollbar.setMomentum(
        0,
        this.scrollbar.limit.y - this.scrollbar.scrollTop
      );
      this.lockY = 1;
    }

    if (y === 0) this.lockY = null;
    if (x === 0) this.lockX = null;
  }
}

// set the plugin name
SoftScrollPlugin.pluginName = "SoftScroll";

/**
 * export the plugins
 */
export { SoftScrollPlugin };

/**
 * # runJellyAnimation
 * To use this animation,
 * you just need to run the function
 */
export function runJellyAnimation() {
  // animation
  // adding jelly scroll effect,
  // will effect all of the element inside this app
  // Soft edges plugin for SmoothScroll
  Scrollbar.use(SoftScrollPlugin);

  // Init smooth scrollbar
  const view = document.getElementById("view-main");
  const scrollbar = Scrollbar.init(view, {
    renderByPixels: false,
    damping: 0.08,
  });

  // Init Jelly and provide smooth scrollbar offset
  // make sure you to add the [data-jelly-scroll] attribute
  // to each section you want to jellying
  const jelly = new JellyEffect({
    intensity: 0.15,
    speed: 0.6,
    min: -5,
    max: 5,
  });

  // Optional demo: pause when scroll via scrollbar track
  scrollbar.track.yAxis.element.addEventListener("mousedown", () => {
    jelly.pause(true);
  });

  // pause teh jelly effect when mouse up
  document.documentElement.addEventListener(
    "mouseup",
    () => {
      jelly.pause(false);
    },
    true
  );
}
