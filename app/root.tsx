import {
  Link,
  Links,
  LinksFunction,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useCatch,
} from "remix";
import type { MetaFunction } from "remix";
import styles from "~/styles/app.css";
import splideStyle from "@splidejs/splide/dist/css/splide.min.css";
import favicon from "~/images/favicon.png";
import { CatchBoundaryComponent } from "@remix-run/react/routeModules";
import { runJellyAnimation } from "~/animations/jelly-animation";
import { runHeroHeaderAnimation } from "~/animations/text-animation";
import { runCursorAnimation } from "~/animations/cursor-animation";
import ContactButtonComponent from "~/components/contact-btn";
import FooterComponent from "~/components/footer";
import HeaderComponent from "~/components/header";
import MenuComponent from "~/components/menu";
import { useEffect } from "react";
import salyimage1 from "~/images/saly-1.png";

/**
 * links
 * link any resource including your css, images, favicon, and other important
 * links
 *
 * TODO: add some links if needed
 * @returns `LinksFunction`
 */
export const links: LinksFunction = () => {
  return [
    {
      rel: "icon",
      href: favicon,
      type: "image/png",
    },
    {
      rel: "stylesheet",
      href: styles,
    },
    {
      rel: "stylesheet",
      href: splideStyle,
    },
  ];
};

/**
 * meta
 * The meta tags settings
 * add some title, description, keyword, facebook opengraph, twitter, and pinterest
 *
 * TODO: add below if needed
 *
 * @returns MetaFunction
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Creative Creators",
    description: "Creative creator for your product, dream, idea",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#0D0E1A",
  };
};

/**
 * ## CatchBoundary
 *
 *
 * use to catch some of the problem
 * useful when something error, notfound in the website
 *
 *
 * TODO: Add some configuration when something error, or not found
 *
 * @returns CatchBoundaryComponent
 */
export const CatchBoundary: CatchBoundaryComponent = () => {
  // get the problem data
  // we will define the actual problem and
  // show the best way to serve it
  const errorCatch = useCatch();

  // run some effect including the animation
  // when the element finished rendered
  useEffect(() => {
    // run jelly animation to each element
    // of this website
    runJellyAnimation();

    // run the cursor animation
    // we going to add the cursor animation
    runCursorAnimation();
  });

  return (
    <html>
      <head>
        <title>Sonibble - Oops, Something Error</title>
        <Meta />
        <Links />
      </head>
      <body>
        {/* header */}
        <HeaderComponent />

        {/* menu */}
        <MenuComponent />

        <main id="view-main">
          {/* bad request error handler */}
          {/* BAD REQUEST */}
          {errorCatch.status == 400 && <div className="error"></div>}

          {/* unauthorize error handler */}
          {/* UNAUTHORIZED */}
          {errorCatch.status == 401 && <div className="error"></div>}

          {/* not found error handler */}
          {/* NOT FOUND */}
          {errorCatch.status == 404 && (
            <section className="error section sn-container sn-mx-auto sn-px-5 md:sn-px-24 sn-my-40  md:sn-mb-96">
              <div className="sn-bg-gray-50 sn-rounded-3xl sn-px-12 sn-py-16 sn-flex sn-flex-row sn-relative">
                <div className="sn-flex sn-flex-col sn-gap-20 sn-w-7/12">
                  <h2 className="sn-font-bold sn-text-black sn-text-6xl">
                    Opps, Not Found
                  </h2>
                  <p className="sn-text-xl text-black sn-leading-9">
                    Opps, your page searched was not found. Please check the
                    url, make sure the url correct and try again. If you seen
                    the same problem after try another solution, feel free to
                    contact us so we can quickly solve the problem
                  </p>

                  <div className="flex sn-mt-20">
                    <Link
                      to="/"
                      className="sn-mt-20 sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                      data-magnetic
                    >
                      <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
                      <span className="sn-relative group-hover:sn-text-white">
                        Go to home
                      </span>
                    </Link>
                  </div>
                </div>

                <div className="sn-hidden md:sn-flex">
                  <img
                    src={salyimage1}
                    alt="Sonbble Saly Image"
                    className="sn-absolute sn-top-0 sn-right-[-10] sn-w-6/12 sn-object-cover"
                  />
                </div>
              </div>
            </section>
          )}
        </main>

        {/* contact button */}
        <ContactButtonComponent />

        {/* footer */}
        <FooterComponent />

        <Scripts />
        {process.env.NODE_ENV === "development" && <LiveReload />}
      </body>
    </html>
  );
};

/**
 * App
 * the main page of app
 * contain many routes,
 * components, and rendering
 *
 *
 * @returns JSX.Element
 */
export default function App() {
  // we need to add some effect for the wesite
  // including the animation, flower, effect and other
  useEffect(() => {
    // add the header animation
    // with mask to all of the header
    runHeroHeaderAnimation();

    // run jelly animation to each element
    // of this website
    runJellyAnimation();

    // run the cursor animation
    // we going to add the cursor animation
    runCursorAnimation();
  });

  return (
    <html lang="en" className="sn-scroll-smooth">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <Meta />
        <Links />
      </head>
      <body className="sn-font-sans sn-text-base sn-text-dark sn-bg-light sn-font-normal">
        {/* SIMPLE LAYOUTING */}
        {/* contain many component */}
        {/* including the header, button, component, and others */}

        {/* header */}
        <HeaderComponent />

        {/* menu */}
        <MenuComponent />

        {/* the childs */}
        <Outlet />

        {/* contact button */}
        <ContactButtonComponent />

        {/* footer */}
        <FooterComponent />

        {/* other elements */}
        <ScrollRestoration />
        <Scripts />
        {process.env.NODE_ENV === "development" && <LiveReload />}
      </body>
    </html>
  );
}
