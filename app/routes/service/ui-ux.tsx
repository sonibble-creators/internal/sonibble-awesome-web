import uiserviceintroimage from "~/images/service/uiux/4.png";
import uiservice1image from "~/images/service/uiux/1.png";
import uiservice2image from "~/images/service/uiux/2.png";
import uiservice3image from "~/images/service/uiux/3.png";

import { Link, MetaFunction } from "remix";

export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - UI UX Service",
    description: "Create a high quality design for your product",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # UIUXServicePage
 *
 * Showing the detail service of ui ux
 * @return JSX.Element
 */
export default function UIUXServicePage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HEADING SECTION */}
      <section
        className="section heading-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-28 md:sn-mt-52"
        data-jelly-scroll
      >
        <span
          className="sn-text-xl sn-font-medium sn-text-black"
          data-cursor-text="Service"
        >
          UI & UX Design
        </span>
        <h2 className="sn-text-3xl md:sn-text-display-2 sn-font-bold sn-mt-11">
          Define your high quality design for awesome product
        </h2>
      </section>

      {/* IMAGE SECTION */}
      <section
        className="section image-section sn-mt-28 md:sn-mt-60"
        data-jelly-scroll
      >
        <img
          src={uiserviceintroimage}
          alt="UI UX Service Sonibble"
          className="sn-w-full"
          data-cursor="before:sn-bg-teal-200"
        />
      </section>

      {/* INTRO SECTION */}
      <section
        className="section intro-section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <p className="md:sn-w-8/12 sn-text-black sn-font-normal sn-text-xl md:sn-text-2xl sn-leading-7 md:sn-leading-10">
          Everytime your user slide, doing slide, touching element inside your
          app they want to feel the experience of your app. Sometimes when your
          feel jank, stack and no innovation users will be leave you app. Now
          define your product with high quality design and improve the
          innovation to keep your user feel confort and you will be the winner
        </p>
        <div className="flex sn-mt-20">
          <Link
            to="/contact"
            className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
            data-magnetic
          >
            <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
            <span className="sn-relative group-hover:sn-text-white">
              Let’s work together
            </span>
          </Link>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5  md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={uiservice1image}
          alt="Sonibble UI UX Service"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-indigo-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto gap-10 sn-px-5 md:sn-px-28 sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="md:sn-w-3/12">
          <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-leading-10">
            Combine everthing to be more reliable
          </h3>
        </div>
        <div className="sn-grow"></div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-left-7 md:sn-leading-10 sn-text-xl md:sn-text-2xl">
            Strategy to bring your product outstanding will be the more
            important. We decide to work with very high pashion and detail in
            each element to make the best product ever.
          </p>
          <p className="sn-text-black sn-left-7 md:sn-leading-10 sn-text-xl md:sn-text-2xl sn-mt-10">
            We create a research, documentation, and look for other competitor
            and then take action what we need, of course will be the briliant
            design appears.
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={uiservice2image}
          alt="Sonibble UI UX Service"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-green-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="md:sn-w-3/12">
          <h3 className="sn-text-2xl md:sn-text-3xl sn-font-medium sn-text-black sn-leading-9">
            Don’t miss your stack will be used
          </h3>
        </div>
        <div className="sn-grow"></div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-leading-7 md:sn-leading-10 sn-text-xl md:sn-text-2xl">
            When design a ui and ux, we will be asked and need to know what
            stack will be use to build your product. It’s really important to
            bring your design easy to impement without other compromising.
          </p>
          <p className="sn-text-black sn-leading-7 md:sn-leading-10 sn-text-xl md:sn-text-2xl sn-mt-10">
            The most complex UI design will have many limitations, sometimes the
            developers can’t implement the actual design and change with other
            options. We will give solutions not a problem
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <img
          src={uiservice3image}
          alt="Sonibble UI UX Service"
          className="sn-w-full"
          data-cursor="before:sn-bg-rose-200"
        />
      </section>

      {/* PROCESS */}
      <section
        className="section sn-container sn-mt-40 md:sn-mt-96 sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col"
        data-jelly-scroll
      >
        <h2
          className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12"
          data-cursor="cursor-lg"
        >
          Where’s the design begin
        </h2>
        <div className="sn-mt-32 md:sn-mt-64 sn-grid sn-grid-cols-1 md:sn-grid-cols-2 sn-gap-20 md:sn-gap-52 sn-w-full">
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black hover:sn-text-stroke-0">
              1
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Briefing, define the problem and goals
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We seriusly bring your problem into solutions. We will hear
              everthin about you, the current problem, your defined goal,
              background, or even the purpose around the project. So we can
              clearly bring it into solutions
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black hover:sn-text-stroke-0">
              2
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Research, bring the best solutions
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Excellent design start with research, peek in many competitors,
              also define the pros and cons in each competitors element. Pushing
              hard to define the solutions, and other resources
            </p>
          </div>

          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black hover:sn-text-stroke-0">
              3
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Sketch & wireframing
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Sketch, and wireframing is one of the most powerful way to
              represent your idea into scratch. So it’s will help the idea
              easily, without worring about other collision. Just go like a
              plan.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black hover:sn-text-stroke-0">
              4
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              UI design process
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Define your design system or create one. Define the element like
              colors, typography, styling, and trend design. We start creating
              your loved design with heart.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black hover:sn-text-stroke-0">
              5
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Workflow design
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Flowing the design including the persona, users, and how the
              application and design will be work. Adding flow to your design to
              make sure you are easy to understand what’s going on.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black hover:sn-text-stroke-0">
              6
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Quality checking
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We seriusly bring your problem into solutions. We will hear
              everthin about you, the current problem, your defined goal,
              background, or even the purpose around the project. So we can
              clearly bring it into solutions
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black hover:sn-text-stroke-0">
              7
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Delivery process
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              The end process to delivery design, we will ask for some feeback,
              and revision until your mind and body satisfied without any
              complain. We try to serve the best service to make sure you are
              the winner
            </p>
          </div>
        </div>
      </section>

      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-my-40 md:sn-my-96"
        data-jelly-scroll
      >
        <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 sn-text-center">
          Have an idea?
        </h2>
        <Link
          to="/contact"
          className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 sn-text-center"
          data-cursor-text="Contact Us"
        >
          Tell about it
        </Link>
      </section>
    </main>
  );
}
