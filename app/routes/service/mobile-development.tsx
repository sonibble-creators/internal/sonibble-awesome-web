import mobiledevserviceimage1 from "~/images/service/mobiledev/1.png";
import mobiledevserviceimage2 from "~/images/service/mobiledev/2.png";
import mobiledevserviceimage3 from "~/images/service/mobiledev/3.png";
import { Link, MetaFunction } from "remix";

export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Mobile Development Service",
    description: "Powering you product using mobile app",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # MobileDevelopmentServicePage
 *
 * Allow user to see the detail mobile development info
 * contain detail data about mobile development
 * @return JSX.Element
 */
export default function MobileDevelopmentServicePage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HEADING SECTION */}
      <section
        className="section heading-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-32 md:sn-mt-52"
        data-jelly-scroll
      >
        <span
          className="sn-text-xl sn-font-medium sn-text-black"
          data-cursor-text="Service"
        >
          Mobile Development
        </span>
        <h2 className="sn-text-3xl md:sn-text-display-2 sn-font-bold sn-mt-11">
          Bring your awesome idea into mobile application
        </h2>
      </section>

      {/* IMAGE SECTION */}
      <section
        className="section image-section sn-mt-28 md:sn-mt-60"
        data-jelly-scroll
      >
        <img
          src={mobiledevserviceimage1}
          alt="Sonibble Mobile Development Service"
          className="sn-w-full"
          data-cursor="before:sn-bg-green-200"
        />
      </section>

      {/* INTRO SECTION */}
      <section
        className="section intro-section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <p className="md:sn-w-8/12 sn-text-black sn-font-normal sn-text-xl md:sn-text-2xl sn-left-7 md:sn-leading-10">
          Every people around the world has their phone, many time spend to hold
          the phone, they life with phone and it’s will always be. Why not
          turning your business and idea into mobile application. The concept is
          handle mobility and easy to doing everthing on your hand. We always
          working and delivery best application for native, and hybrid mobile
          application, no matter what’s your idea we can help
        </p>
        <div className="flex sn-mt-20">
          <Link
            to="/contact"
            className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
            data-magnetic
          >
            <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
            <span className="sn-relative group-hover:sn-text-white">
              Let's work together
            </span>
          </Link>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={mobiledevserviceimage2}
          alt="Sonibble Mobile Development Service"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-pink-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="md:sn-w-3/12">
          <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-leading-10">
            Make your life like in heaven
          </h3>
        </div>
        <div className="sn-grow"></div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-leading-7 md:sn-leading-10 ns sn-text-xl md:sn-text-2xl">
            When developing your application, we always working out of
            possibility, pashionate, and details. We try to serve the best
            service to make an clean, fast, and performance application.
          </p>
          <p className="sn-text-black sn-leading-7 md:sn-leading-10 ns sn-text-xl md:sn-text-2xl sn-mt-10">
            No matter what you idea, you will get a high quality application
            including fully support for every bugs come out. We ensure your
            experience working with us is awesome
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <img
          src={mobiledevserviceimage3}
          alt=""
          className="sn-w-full"
          data-cursor="before:sn-bg-rose-200"
        />
      </section>

      {/* PROCESS */}
      <section
        className="section sn-container sn-mt-40 md:sn-mt-96 sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col"
        data-jelly-scroll
      >
        <h2
          className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12"
          data-cursor="cursor-lg"
        >
          Where’s the development begin
        </h2>
        <div className="sn-mt-32 md:sn-mt-64 sn-grid sn-grid-cols-1 md:sn-grid-cols-2 sn-gap-20 md:sn-gap-52 sn-w-full">
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              1
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Briefing & handoff UI design
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We always take care about the project background, purpose. Start
              with slicing, handoff the ui design including assets, typography,
              colors, images. We will define the all assets needed before jump
              more details.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              2
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Define the best stack and platform
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              See the complexity, and problem, solution, design we will decice
              which stack and platform to be used to develop your applications
              including the limitations in each stack.
            </p>
          </div>

          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              3
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Initial mobile app project
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Create initial project that including the git version controlling.
              so we can easily collaborate each other. Of course we will add
              come assets into the app like colors, image, icons, and
              typography.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              4
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Bring ui design life
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We create the user interface based on ui design. We will make the
              design like same like you see, no different and more intuitive. Of
              source will bring some animations to make awesome.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              5
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Logic implement
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Now will be the logic time, we will implement the backend with
              your application using secure, fast, and the newest methode to
              ensure the performace, and as fast as possible.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              6
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Quality assurance
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We doing end to end continues intergration, and delivery to
              testing the application and make sure the quality app is the best
              we can do.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              7
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Release
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We’re help to launching your application in Google play store and
              Apps Store including their promotions, screenshot and other
              material needed
            </p>
          </div>
        </div>
      </section>

      <section
        className="section sn-container sn-mx-auto px-5 md:sn-px-28 sn-flex sn-flex-col sn-my-40 md:sn-my-96"
        data-jelly-scroll
      >
        <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 sn-text-center">
          Have an idea?
        </h2>
        <Link
          to="/contact"
          className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 sn-text-center"
        >
          Tell about it
        </Link>
      </section>
    </main>
  );
}
