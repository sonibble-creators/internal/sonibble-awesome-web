import devopsdevserviceimage1 from "~/images/service/devops/1.png";
import devopsdevserviceimage2 from "~/images/service/devops/2.png";
import devopsdevserviceimage3 from "~/images/service/devops/3.png";
import { Link, MetaFunction } from "remix";

export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - DevOps Development Service",
    description: "Charge your product with high quality backend",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # DevOpsDevelopmentServicePage
 *
 * Allow user to see the detail of DevOps development service
 * @returns JSX.Element
 */
export default function DevOpsDevelopmentServicePage() {
  return (
    <main id="view-main">
      {/* HEADING SECTION */}
      <section
        className="section heading-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-32 md:sn-mt-52"
        data-jelly-scroll
      >
        <span
          className="sn-text-xl sn-font-medium sn-text-black"
          data-cursor-text="Service"
        >
          Backend & Dev Ops Development
        </span>
        <h2 className="sn-text-3xl md:sn-text-display-2 sn-font-bold sn-mt-11">
          Super power backed for your awesome products
        </h2>
      </section>

      {/* IMAGE SECTION */}
      <section
        className="section image-section sn-mt-28 md:sn-mt-60"
        data-jelly-scroll
      >
        <img
          src={devopsdevserviceimage1}
          alt="Sonibble DevOps Development Service"
          className="sn-w-full"
          data-cursor="before:sn-bg-teal-200"
        />
      </section>

      {/* INTRO SECTION */}
      <section
        className="section intro-section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <p className="md:sn-w-8/12 sn-text-black sn-font-normal sn-text-xl md:sn-text-2xl sn-left-6 md:sn-leading-10">
          Whatever your products, you always need fast, powerful, and efficient
          backend. We offer you most powerful design backend with many
          integration including and advance setting. So you can consume
          everthing using your way.
        </p>
        <div className="flex sn-mt-20">
          <Link
            to="/contact"
            className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
            data-magnetic
          >
            <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
            <span className="sn-relative group-hover:sn-text-white">
              Let's work together
            </span>
          </Link>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={devopsdevserviceimage2}
          alt=""
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-pink-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-gap-10 sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="md:sn-w-3/12">
          <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-leading-10">
            Pick your need, and deploy everywhere
          </h3>
        </div>
        <div className="sn-grow"></div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-leading-7 md:sn-leading-10 sn-text-xl md:sn-text-2xl">
            We building your backend with fully startegy, start with define the
            problem, and mining every solutions, until we can decide whic
            architecture will be used and which platform.
          </p>
          <p className="sn-text-black sn-left-7 md:sn-leading-10 sn-text-xl md:sn-text-2xl sn-mt-10">
            When we working on backend we always ensure the backend work
            properly and without issue, also has an ability to handle a million
            people every miliseconds. We alwasy working with ci cd to ensure the
            process faster and reliable
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section className="section sn-mt-40 md:sn-mt-96" data-jelly-scroll>
        <img
          src={devopsdevserviceimage3}
          alt="Sonibble DevOps Development Service"
          className="sn-w-full"
          data-cursor="before:sn-bg-rose-200"
        />
      </section>

      <section
        className="section sn-container sn-mt-40 md:sn-mt-96 sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col"
        data-jelly-scroll
      >
        <h2
          className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12"
          data-cursor="cursor-lg"
        >
          Where’s the development begin
        </h2>
        <div className="sn-mt-20 md:sn-mt-64 sn-grid sn-grid-cols-1 md:sn-grid-cols-2 sn-gap-32 md:sn-gap-52 sn-w-full">
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              1
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Briefing & define problem, solutions
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We always start from the project problems, and the background. so
              we can easily to find the solutions including the way we can do.
              Ensure your purpose and mean es alwasy the number one we need.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              2
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Define the best stack and platform
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Choosing your best stack and platform to make sure your backend
              become super power, fast, and give scallability. So when you need
              to improve the power you can do it by less effort.
            </p>
          </div>

          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              3
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Structure design
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Define the architecture, what we need, and other integrations
              needed depend on your product requirements. So we can easily make
              the project run faster and maintainable.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              4
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Database design
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Design the database, relations, and other key or reference we
              need. When the projec will be scalling the database will be ready
              for any change and improvment.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              5
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              API design
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              Pick the best solution and technology for your API. We design it
              for many thing like easy to use, maintain, and deprecation. So
              when other api comes out the api will be documented perfectly.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              6
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Quality assurance
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We doing end to end continues intergration, and delivery to
              testing the backend and make sure the quality is the best we can
              do.
            </p>
          </div>
          <div
            className="sn-flex sn-flex-col sn-group"
            data-magnetic
            data-cursor="cursor-opaque"
          >
            <span className="sn-font-bold sn-text-stroke-black sn-text-stroke-2 sn-text-8xl sn-text-transparent sn-transition-all sn-duration-700 group-hover:sn-text-black group-hover:sn-text-stroke-0">
              7
            </span>

            <h3 className="sn-text-3xl sn-font-medium sn-text-black sn-mt-12">
              Deploy
            </h3>
            <p className="sn-text-lg sn-text-black sn-mt-10 sn-leading-8">
              We’re help to deploy your backend on many platform your choose. We
              also support for other technology integration so it’s will be
              healpfuly to make your backend become the complete one.
            </p>
          </div>
        </div>
      </section>

      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-my-40 md:sn-my-96"
        data-jelly-scroll
      >
        <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 sn-text-center">
          Have an idea?
        </h2>
        <Link
          to="/contact"
          className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 sn-text-center"
          data-cursor-text="Contact Us"
        >
          Tell about it
        </Link>
      </section>
    </main>
  );
}
