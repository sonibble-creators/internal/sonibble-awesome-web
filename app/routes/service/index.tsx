import uiuxserviceimage from "~/images/service/uiux.png";
import mobiledevserviceimage from "~/images/service/mobile-dev.png";
import webdevserviceimage from "~/images/service/webdev.png";
import devopssersviceimage from "~/images/service/devops.png";
import { Link, MetaFunction } from "remix";

export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Service",
    description: "Awesome service provided for your product, dream",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # ServicePage
 * Allow user to see all of the service
 * provided
 * contain many information about the service
 * @return JSX.Element
 */
export default function ServicePage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HERO SECTION */}
      {/* use in every type of page with header and introductions */}
      {/* it will contain some animation like hover effect and masking the text */}
      <section
        className="sn-flex sn-flex-col sn-container sn-mx-auto sn-pt-20 md:sn-pt-32 sn-mb-32 md:sn-pb-52"
        data-jelly-scroll
        data-cursor="before:sn-bg-purple-500"
      >
        <div className="sn-flex hero-headline-animated sn-flex-col sn-items-center sn-relative sn-overflow-hidden">
          {/* add the shape for masking the text  */}
          {/* will be there, with some feature and shapes rect */}
          <div className="hero-headline-shape sn-bg-black sn-absolute sn-inset-5">
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[50vw] sn-h-[50vw] sn-rounded-full sn-z-30 sn-bg-[#4f2cee] sn-ml-[calc(-50vw/2)] sn-mt-[calc(-50vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[35vw] sn-h-[35vw] sn-rounded-full sn-z-30 sn-bg-[#cf38fd] sn-ml-[calc(-35vw/2)] sn-mt-[calc(-35vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[20vw] sn-h-[20vw] sn-rounded-full sn-z-30 sn-bg-[#fdc838] sn-ml-[calc(-20vw/2)] sn-mt-[calc(-20vw/2)] sn-mix-blend-screen"></div>
          </div>

          {/* the text with some animation comes */}
          <div className="hero-headline-text sn-flex sn-flex-col sn-justify-center sn-mix-blend-screen sn-bg-white sn-w-full">
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              Going beyond
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              what’s possible with
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              creative way
            </span>
          </div>
        </div>
      </section>

      {/* INTRO */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-40 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-2xl">
            What’s the creators do
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-lg md:sn-text-2xl md:sn-leading-10 sn-font-normal">
            A strategic and tactical goal to reach the best products and
            services, but the mor important is collaboration and ensure the
            quality, suport for better experiences
          </p>
        </div>
      </section>

      {/* UI & UX DESIGN */}
      <section
        className="section uiux-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-3xl md:sn-text-display-2 sn-text-center md:sn-w-6/12"
            data-cursor="cursor-lg"
          >
            UI & UX design
          </h2>
        </div>
        <div className="content sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-32 md:sn-mt-72">
          <div className="md:sn-w-4/12">
            <Link to="ui-ux">
              <img
                src={uiuxserviceimage}
                alt="UI & UX Service Sonibble"
                className="sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Read More"
                data-cursor="before:sn-bg-pink-200"
              />
            </Link>
          </div>
          <div className="sn-grow"></div>
          <div className="md:sn-w-6/12 sn-flex sn-flex-col sn-justify-center">
            <p className="sn-text-black sn-text-lg md:sn-text-xl md:sn-leading-9">
              Design is more crucial than other, Every user spend more time into
              your product. They expect to deep dive and exploring your design,
              experience. So don’t waste it, create awesome UI UX for your
              product
            </p>

            <div className="flex sn-mt-16">
              <Link
                to="ui-ux"
                className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                data-magnetic
              >
                <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
                <span className="sn-relative group-hover:sn-text-white">
                  Read More
                </span>
              </Link>
            </div>
          </div>
        </div>
      </section>

      {/* MOBILE DEVELOPMENT */}
      <section
        className="section mobile-development-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-3xl md:sn-text-display-2 sn-text-center md:sn-w-6/12"
            data-cursor="cursor-lg"
          >
            Mobile development
          </h2>
        </div>
        <div className="content sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-40 md:sn-mt-72">
          <div className="md:sn-w-6/12 sn-flex sn-flex-col sn-justify-center">
            <p className="sn-text-black sn-text-lg md:sn-text-xl md:sn-leading-9">
              High quality application and depend on excellent UI UX Design. The
              result is to ensure perfection, running blowing, and ensure
              suitable with your users to reach your own goals
            </p>

            <div className="flex sn-mt-16">
              <Link
                to="mobile-development"
                className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                data-magnetic
              >
                <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
                <span className="sn-relative group-hover:sn-text-white">
                  Read More
                </span>
              </Link>
            </div>
          </div>
          <div className="sn-grow"></div>
          <div className="md:sn-w-4/12">
            <Link to="mobile-development">
              <img
                src={mobiledevserviceimage}
                alt="Mobile Development Service Sonibble"
                className="sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Read More"
                data-cursor="before:sn-bg-green-200"
              />
            </Link>
          </div>
        </div>
      </section>

      {/* WEB DEVELOPMENT SECTION */}
      <section
        className="section web-development-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-3xl md:sn-text-display-2 sn-text-center md:sn-w-6/12"
            data-cursor="cursor-lg"
          >
            Web development
          </h2>
        </div>
        <div className="content sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-32 md:sn-mt-72">
          <div className="md:sn-w-4/12">
            <Link to="web-development">
              <img
                src={webdevserviceimage}
                alt=""
                className=""
                data-cursor-text="Read More"
                data-cursor="before:sn-bg-orange-200"
              />
            </Link>
          </div>
          <div className="sn-grow"></div>
          <div className="md:sn-w-6/12 sn-flex sn-flex-col sn-justify-center">
            <p className="sn-text-black sn-text-lg md:sn-text-xl md:sn-leading-9">
              Expect fast, and reliable web for your app and identity. Creators
              work alongside with design so will easy to implement your unique
              design into your own dream products.
            </p>

            <div className="flex sn-mt-16">
              <Link
                to="web-development"
                className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                data-magnetic
              >
                <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
                <span className="sn-relative group-hover:sn-text-white">
                  Read More
                </span>
              </Link>
            </div>
          </div>
        </div>
      </section>

      {/* BACKEND & DEV OPS DEVELOPMENT */}
      <section
        className="section backend-devops-development-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-3xl md:sn-text-display-2 sn-text-center md:sn-w-6/12"
            data-cursor="cursor-lg"
          >
            Backend & Dev Ops development
          </h2>
        </div>
        <div className="content sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-32 md:sn-mt-72">
          <div className="md:sn-w-6/12 sn-flex sn-flex-col sn-justify-center">
            <p className="sn-text-black sn-text-xl sn-leading-9">
              Ensure your application quality with strong performance backend.
              Improve the Continues Deivery and Integration with experience hand
              to handle the hard tasks
            </p>

            <div className="flex sn-mt-16">
              <Link
                to="devops-development"
                className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                data-magnetic
              >
                <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
                <span className="sn-relative group-hover:sn-text-white">
                  Read More
                </span>
              </Link>
            </div>
          </div>
          <div className="sn-grow"></div>

          <div className="md:sn-w-4/12">
            <Link to="devops-development">
              <img
                src={devopssersviceimage}
                alt="DevOps Development Service Sonibble"
                className="sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Read More"
                data-cursor="before:sn-bg-indigo-200"
              />
            </Link>
          </div>
        </div>
      </section>

      {/* BELIEF SECTION */}
      <section
        className="section outro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-96 sn-pb-40 md:sn-pb-96"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-3xl md:sn-text-display-2 sn-text-center"
            data-cursor-text="Belief"
          >
            Carry about your problem and solve with the best solution provided
          </h2>
        </div>
      </section>
    </main>
  );
}
