import { MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @returns MetaFunction
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Privacy Policy",
    description: "See all about the privacy policy sonibble",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # PrivacyPage
 *
 * Show the privacy policy
 * information, user data, cookies, and other site refer
 * @return JSX.Element
 */
export default function PrivacyPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HERO SECTION */}
      {/* use in every type of page with header and introductions */}
      {/* it will contain some animation like hover effect and masking the text */}
      <section
        className="sn-flex sn-flex-col sn-container sn-mx-auto sn-pt-20  md:sn-pt-32 sn-pb-32 md:sn-pb-52"
        data-jelly-scroll
        data-cursor="before:sn-bg-purple-500"
      >
        <div className="sn-flex hero-headline-animated sn-flex-col sn-items-center sn-relative sn-overflow-hidden">
          {/* add the shape for masking the text  */}
          {/* will be there, with some feature and shapes rect */}
          <div className="hero-headline-shape sn-bg-black sn-absolute sn-inset-5">
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[50vw] sn-h-[50vw] sn-rounded-full sn-z-30 sn-bg-[#4f2cee] sn-ml-[calc(-50vw/2)] sn-mt-[calc(-50vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[35vw] sn-h-[35vw] sn-rounded-full sn-z-30 sn-bg-[#cf38fd] sn-ml-[calc(-35vw/2)] sn-mt-[calc(-35vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[20vw] sn-h-[20vw] sn-rounded-full sn-z-30 sn-bg-[#fdc838] sn-ml-[calc(-20vw/2)] sn-mt-[calc(-20vw/2)] sn-mix-blend-screen"></div>
          </div>

          {/* the text with some animation comes */}
          <div className="hero-headline-text sn-flex sn-flex-col sn-justify-center sn-mix-blend-screen sn-bg-white sn-w-full">
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              Privacy
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              Policy
            </span>
          </div>
        </div>
      </section>

      {/* CONTENT */}
      <section
        className="sn-px-5 md:sn-w-7/12 sn-mx-auto sn-flex sn-flex-col sn-mt-20 md:sn-mt-40 sn-mb-40 md:sn-mb-96"
        data-jelly-scroll
      >
        <span className="sn-text-lg md:sn-text-xl sn-text-black sn-leading-9 sn-font-normal sn-mt-8">
          Effective date: January 30, 2022
        </span>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Sonibble ("us", "we", or "our", "team") operates the
          <a
            href="https://sonibble.com"
            target="_blank"
            className="sn-font-medium sn-mx-2"
          >
            https://sonibble.com
          </a>
          website (the "Service").
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          This page informs you of our policies regarding the collection, use,
          and disclosure of personal data when you use our Service and the
          choices you have associated with that data.
        </p>

        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We use your data to provide and improve the Service. By using the
          Service, you agree to the collection and use of information in
          accordance with this policy. Unless otherwise defined in this Privacy
          Policy, terms used in this Privacy Policy have the same meanings as in
          our Terms and Conditions, accessible from https://sonibble.com
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Information Collection And Use
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We collect several different types of information for various purposes
          to provide and improve our Service to you.
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Types of Data Collected
        </h2>
        <h3 className="sn-text-2xl sn-text-black sn-font-medium sn-mt-12 sn-mb-8">
          Personal Data
        </h3>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          While using our Service, we may ask you to provide us with certain
          personally identifiable information that can be used to contact or
          identify you ("Personal Data"). Personally identifiable information
          may include, but is not limited to:
        </p>
        <ul className="sn-list-disc sn-ml-10 sn-mt-12">
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            Email address
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            First name and last name
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            Cookies and Usage Data
          </li>
        </ul>

        <h3 className="sn-text-2xl sn-text-black sn-font-medium sn-mt-12 sn-mb-8">
          Usage Data
        </h3>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We may also collect information how the Service is accessed and used
          ("Usage Data"). This Usage Data may include information such as your
          computer's Internet Protocol address (e.g. IP address), browser type,
          browser version, the pages of our Service that you visit, the time and
          date of your visit, the time spent on those pages, unique device
          identifiers and other diagnostic data.
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Tracking & Cookies Data
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We use cookies and similar tracking technologies to track the activity
          on our Service and hold certain information.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Cookies are files with small amount of data which may include an
          anonymous unique identifier. Cookies are sent to your browser from a
          website and stored on your device. Tracking technologies also used are
          beacons, tags, and scripts to collect and track information and to
          improve and analyze our Service.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          You can instruct your browser to refuse all cookies or to indicate
          when a cookie is being sent. However, if you do not accept cookies,
          you may not be able to use some portions of our Service.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Examples of Cookies we use:
        </p>
        <ul className="sn-list-disc sn-ml-10 sn-mt-12">
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            Session Cookies. We use Session Cookies to operate our Service.
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            Preference Cookies. We use Preference Cookies to remember your
            preferences and various settings.
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            ecurity Cookies. We use Security Cookies for security purposes.
          </li>
        </ul>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Use of Data
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Sonibble Inc uses the collected data for various purposes:
        </p>
        <ul className="sn-list-disc sn-ml-10 sn-mt-12">
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To provide and maintain the Service
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To notify you about changes to our Service
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To allow you to participate in interactive features of our Service
            when you choose to do so
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To provide customer care and support
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To provide analysis or valuable information so that we can improve
            the Service
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To monitor the usage of the Service
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To detect, prevent and address technical issues
          </li>
        </ul>

        <h3 className="sn-text-2xl sn-text-black sn-font-medium sn-mt-12 sn-mb-8">
          Transfer Of Data
        </h3>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Your information, including Personal Data, may be transferred to — and
          maintained on — computers located outside of your state, province,
          country or other governmental jurisdiction where the data protection
          laws may differ than those from your jurisdiction.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          If you are located outside United States and choose to provide
          information to us, please note that we transfer the data, including
          Personal Data, to United States and process it there.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Your consent to this Privacy Policy followed by your submission of
          such information represents your agreement to that transfer.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Sonibble Inc will take all steps reasonably necessary to ensure that
          your data is treated securely and in accordance with this Privacy
          Policy and no transfer of your Personal Data will take place to an
          organization or a country unless there are adequate controls in place
          including the security of your data and other personal information.
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Disclosure Of Data
        </h2>
        <h3 className="sn-text-2xl sn-text-black sn-font-medium sn-mt-12 sn-mb-8">
          Legal Requirements
        </h3>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Sonibble Inc may disclose your Personal Data in the good faith belief
          that such action is necessary to:
        </p>

        <ul className="sn-list-disc sn-ml-10 sn-mt-12">
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To comply with a legal obligation
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To protect and defend the rights or property of Sonibble Inc
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To prevent or investigate possible wrongdoing in connection with the
            Service
          </li>

          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To protect the personal safety of users of the Service or the public
          </li>
          <li className="sn-list-item sn-text-lg md:sn-text-xl sn-text-black sn-font-normal sn-leading-9 sn-mt-2">
            To protect against legal liability
          </li>
        </ul>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Security Of Data
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          The security of your data is important to us, but remember that no
          method of transmission over the Internet, or method of electronic
          storage is 100% secure. While we strive to use commercially acceptable
          means to protect your Personal Data, we cannot guarantee its absolute
          security.
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Service Providers
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We may employ third party companies and individuals to facilitate our
          Service ("Service Providers"), to provide the Service on our behalf,
          to perform Service-related services or to assist us in analyzing how
          our Service is used.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          These third parties have access to your Personal Data only to perform
          these tasks on our behalf and are obligated not to disclose or use it
          for any other purpose.
        </p>

        <h3 className="sn-text-2xl sn-text-black sn-font-medium sn-mt-12 sn-mb-8">
          Analytics
        </h3>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We may use third-party Service Providers to monitor and analyze the
          use of our Service.
        </p>

        <h3 className="sn-text-2xl sn-text-black sn-font-medium sn-mt-12 sn-mb-8">
          Google Analytics
        </h3>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Google Analytics is a web analytics service offered by Google that
          tracks and reports website traffic. Google uses the data collected to
          track and monitor the use of our Service. This data is shared with
          other Google services. Google may use the collected data to
          contextualize and personalize the ads of its own advertising network.
        </p>

        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          You can opt-out of having made your activity on the Service available
          to Google Analytics by installing the Google Analytics opt-out browser
          add-on. The add-on prevents the Google Analytics JavaScript (ga.js,
          analytics.js, and dc.js) from sharing information with Google
          Analytics about visits activity.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          For more information on the privacy practices of Google, please visit
          the Google Privacy & Terms web page:
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          <a
            href="https://policies.google.com/privacy"
            target="_blank"
            className="sn-font-medium sn-mx-2"
          >
            https://policies.google.com/privacy
          </a>
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Links To Other Sites
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Our Service may contain links to other sites that are not operated by
          us. If you click on a third party link, you will be directed to that
          third party's site. We strongly advise you to review the Privacy
          Policy of every site you visit.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We have no control over and assume no responsibility for the content,
          privacy policies or practices of any third party sites or services.
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Children's Privacy
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          Our Service does not address anyone under the age of 18 ("Children").
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We do not knowingly collect personally identifiable information from
          anyone under the age of 18. If you are a parent or guardian and you
          are aware that your Children has provided us with Personal Data,
          please contact us. If we become aware that we have collected Personal
          Data from children without verification of parental consent, we take
          steps to remove that information from our servers.
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Changes To This Privacy Policy
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We may update our Privacy Policy from time to time. We will notify you
          of any changes by posting the new Privacy Policy on this page.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          We will let you know via email and/or a prominent notice on our
          Service, prior to the change becoming effective and update the
          "effective date" at the top of this Privacy Policy.
        </p>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          You are advised to review this Privacy Policy periodically for any
          changes. Changes to this Privacy Policy are effective when they are
          posted on this page.
        </p>

        <h2 className="sn-text-black sn-text-3xl md:sn-text-5xl sn-font-medium sn-mt-16 sn-mb-8">
          Contact Us
        </h2>
        <p className="sn-text-lg md:sn-text-xl sn-font-normal sn-leading-9 sn-text-black sn-mt-8">
          If you have any questions about this Privacy Policy, please contact us
          by email:
          <a
            href="mailto:creative.sonibble@gmail.com"
            target="_blank"
            className="sn-font-medium sn-mx-2"
          >
            creative.sonibble@gmail.com
          </a>
        </p>
      </section>
    </main>
  );
}
