import { Link, MetaFunction } from "remix";
import devooimage from "~/images/product/devoo.png";
import desenoimage from "~/images/product/deseno.png";

/**
 * meta
 *
 * allow to add meta tags into website
 *
 * @returns MetaFunction
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Products",
    description: "Explore the greatest product",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # ProductPage
 *
 * Allow user to see the detail of product
 * @return JSX.Element
 */
export default function ProductPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HERO SECTION */}
      {/* use in every type of page with header and introductions */}
      {/* it will contain some animation like hover effect and masking the text */}
      <section
        className="hero-section sn-flex sn-flex-col sn-container sn-mx-auto sn-pt-20 md:sn-pt-32 sn-pb-32 md:sn-pb-52"
        data-jelly-scroll
        data-cursor="before:sn-bg-purple-500"
      >
        <div className="sn-flex hero-headline-animated sn-flex-col sn-items-center sn-relative sn-overflow-hidden">
          {/* add the shape for masking the text  */}
          {/* will be there, with some feature and shapes rect */}
          <div className="hero-headline-shape sn-bg-black sn-absolute sn-inset-0">
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[50vw] sn-h-[50vw] sn-rounded-full sn-z-30 sn-bg-[#4f2cee] sn-ml-[calc(-50vw/2)] sn-mt-[calc(-50vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[35vw] sn-h-[35vw] sn-rounded-full sn-z-30 sn-bg-[#cf38fd] sn-ml-[calc(-35vw/2)] sn-mt-[calc(-35vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[20vw] sn-h-[20vw] sn-rounded-full sn-z-30 sn-bg-[#fdc838] sn-ml-[calc(-20vw/2)] sn-mt-[calc(-20vw/2)] sn-mix-blend-screen"></div>
          </div>

          {/* the text with some animation comes */}
          <div className="hero-headline-text sn-flex sn-flex-col sn-justify-center sn-mix-blend-screen sn-bg-white sn-z-20 sn-w-full">
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              Explore the amazing
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              products that suited on
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              your need
            </span>
          </div>
        </div>
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-my-40 md:sn-my-80"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-3xl md:sn-text-display-2 sn-text-center"
            data-cursor="cursor-lg"
          >
            We build all of our product with simple, creative and big heart
          </h2>
        </div>
      </section>

      {/* Devoo Product */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-96"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-3xl md:sn-text-display-2 sn-text-center md:sn-w-6/12"
            data-cursor="cursor-lg"
          >
            Devoo
          </h2>
        </div>
        <div className="content sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-32 md:sn-mt-72">
          <div className="md:sn-w-4/12">
            <Link to="">
              <img
                src={devooimage}
                alt="Devoo Product Sonibble"
                className="sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Coming Soon"
                data-cursor="before:sn-bg-indigo-200"
              />
            </Link>
          </div>
          <div className="sn-grow"></div>
          <div className="md:sn-w-6/12 sn-flex sn-flex-col sn-justify-center">
            <p className="sn-text-black sn-text-lg md:sn-text-xl sn-leading-9">
              Devoo is a learning platform for everyone to become developer
              expert. Comes with many feature and free for all member, no money,
              no card needed. You can see and explore everwhere you are, we are
              come in many platform you are used.
            </p>

            <div className="flex sn-mt-16">
              <Link
                to=""
                className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                data-magnetic
              >
                <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
                <span className="sn-relative group-hover:sn-text-white">
                  Coming Soon
                </span>
              </Link>
            </div>
          </div>
        </div>
      </section>

      {/* DESENO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-96 sn-mb-40 md:sn-mb-96"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-3xl md:sn-text-display-2 sn-text-center md:sn-w-6/12"
            data-cursor="cursor-lg"
          >
            Deseno
          </h2>
        </div>
        <div className="content sn-flex sn-flex-col md:sn-flex-row sn-gap-10 sn-mt-32 md:sn-mt-72">
          <div className="md:sn-w-6/12 sn-flex sn-flex-col sn-justify-center">
            <p className="sn-text-black sn-text-xl sn-leading-9">
              Deseno is a learning platform for everyone to become designer
              expert. Comes with many feature and free for all member, no money,
              no card needed. You can see and explore everwhere you are, we are
              come in many platform you are used.
            </p>

            <div className="flex sn-mt-16">
              <Link
                to=""
                className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                data-magnetic
              >
                <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
                <span className="sn-relative group-hover:sn-text-white">
                  Coming Soon
                </span>
              </Link>
            </div>
          </div>
          <div className="sn-grow"></div>
          <div className="md:sn-w-4/12">
            <Link to="">
              <img
                src={desenoimage}
                alt="Deseno Product Sonibble"
                className="sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Coming Soon"
                data-cursor="before:sn-bg-cyan-200"
              />
            </Link>
          </div>
        </div>
      </section>
    </main>
  );
}
