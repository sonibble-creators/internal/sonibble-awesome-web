import aboutimage1 from "~/images/about/1.png";
import aboutimage2 from "~/images/about/2.png";
import inspirationonboardimage from "~/images/inspiration-onboard-item.png";
import inspirationweatherimage from "~/images/inspiration-weather-item.png";
import inspirationcardimage from "~/images/inspiration-card-item.png";
import inspirationotherimage from "~/images/inspiration-other-item.png";
import { Icon } from "@iconify/react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @returns MetaFunction
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - About",
    description: "Knowing what's behind sonibble and everting you see now",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # AboutPage
 * Show the about section
 * data from about
 * @return JSX.Element
 */
export default function AboutPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HERO SECTION */}
      {/* use in every type of page with header and introductions */}
      {/* it will contain some animation like hover effect and masking the text */}
      <section
        className="sn-flex sn-flex-col sn-container sn-mx-auto sn-pt-10 md:sn-pt-32 sn-pb-24 md:sn-pb-52"
        data-jelly-scroll
        data-cursor="before:sn-bg-purple-500"
      >
        <div className="sn-flex hero-headline-animated sn-flex-col sn-items-center sn-relative sn-overflow-hidden">
          {/* add the shape for masking the text  */}
          {/* will be there, with some feature and shapes rect */}
          <div className="hero-headline-shape sn-bg-black sn-absolute sn-inset-5">
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[50vw] sn-h-[50vw] sn-rounded-full sn-z-30 sn-bg-[#4f2cee] sn-ml-[calc(-50vw/2)] sn-mt-[calc(-50vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[35vw] sn-h-[35vw] sn-rounded-full sn-z-30 sn-bg-[#cf38fd] sn-ml-[calc(-35vw/2)] sn-mt-[calc(-35vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[20vw] sn-h-[20vw] sn-rounded-full sn-z-30 sn-bg-[#fdc838] sn-ml-[calc(-20vw/2)] sn-mt-[calc(-20vw/2)] sn-mix-blend-screen"></div>
          </div>

          {/* the text with some animation comes */}
          <div className="hero-headline-text sn-flex sn-flex-col sn-justify-center sn-mix-blend-screen sn-bg-white sn-w-full">
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              Creativity
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              meets technology
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              and collaboration
            </span>
          </div>
        </div>
      </section>

      {/* ABOUT IMAGE */}
      {/* the representate of image for sonibble logo */}
      <section className="section sn-mt-20 md:sn-mt-40" data-jelly-scroll>
        <img
          src={aboutimage1}
          alt="Sonibble"
          className="sn-w-full"
          data-cursor="before:sn-bg-orange-200"
        />
      </section>

      {/* MISSION */}
      {/* tell about the mission */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-my-20 md:sn-my-80"
        data-jelly-scroll
      >
        <div className="heading sn-flex sn-justify-center">
          <h2
            className="sn-font-bold sn-text-black sn-text-2xl md:sn-text-display-2 sn-text-center"
            data-cursor-text="Mission"
            data-cursor="cursor-lg"
          >
            Our mission is to make awesome product and help people around the
            world
          </h2>
        </div>
      </section>

      {/* GOALS */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-40 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-x-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-2xl">Our goal</h3>
        </div>
        <div className="md:sn-w-7/12 sn-flex sn-flex-col sn-gap-10">
          <p className="sn-text-black sn-text-lg md:sn-text-2xl md:sn-leading-10 sn-font-normal">
            We want to help people to create a memorable and expensive products.
            Create an opportunity and help people to knowing the digital world
            by educate.
          </p>
          <p className="sn-text-black sn-text-lg md:sn-text-2xl md:sn-leading-10 sn-font-normal">
            Our expertise grows in every year so we can help more people by
            sharing and understand and solving the world problems.
          </p>
        </div>
      </section>

      {/* ABOUT BEHIND */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex md:sn-flex-row sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="content sn-flex sn-flex-col md:sn-flex-row sn-gap-y-10 md:sn-gap-x-10 sn-mt-12 md:sn-mt-72 sn-items-center">
          <div className="md:sn-w-4/12">
            <img
              src={aboutimage2}
              alt="Nyoman Sunima Founder Sonibble"
              className=""
              data-cursor="before:sn-bg-orange-200"
            />
          </div>
          <div className="sn-grow"></div>
          <div className="md:sn-w-6/12 sn-flex sn-flex-col sn-justify-center sn-gap-10">
            <div className="flex sn-flex-col">
              <h2 className="sn-text-black sn-font-bold sn-text-2xl md:sn-text-display-2">
                Get’s to know
              </h2>
              <h2 className="sn-text-black sn-font-bold sn-text-2xl md:sn-text-display-2">
                How it’s happen
              </h2>
            </div>
            <p className="sn-text-black sn-text-base md:sn-text-xl md:sn-leading-9">
              Nyoman Sunima bring an idea to help the people around the world by
              creating best services and product. Sonibble comes in 2020 when
              there’s so much problem, and want to solve and make more valuable
              products. Now it’s just create by single person.
            </p>
            <p className="sn-text-black sn-text-base md:sn-text-xl md:sn-leading-9">
              It’s just the beginning, we’re currently want to make more
              memorable and don’t forget to educate and sharing whhat we know
              today to others. We always open to other feedback to become
              perfect.
            </p>
          </div>
        </div>
      </section>

      {/* INSPIRATION SECTION */}
      {/* contain inspiration post and other resource */}
      <section
        className="section sn-flex sn-flex-col sn-mt-40 md:sn-mt-80 sn-mb-40 md:sn-mb-80"
        data-jelly-scroll
      >
        <div className="heading-section sn-flex sn-flex-col sn-container sn-mx-auto sn-px-5 md:sn-px-28">
          <div className="headline-section sn-flex sn-flex-col md:sn-w-2/3">
            <span className="sn-text-2xl md:sn-text-display-2 sn-text-black sn-font-bold">
              New day
            </span>
            <span className="sn-text-2xl md:sn-text-display-2 sn-text-black sn-font-bold">
              new inpirations
            </span>
          </div>
          <div className="headline-description-section sn-flex sn-justify-start sn-mt-10 md:sn-w-2/3">
            <p className="sn-text-black sn-font-normal sn-text-base md:sn-text-2xl">
              Update with some inspiration. Let you mind blow up and bring
              creative idea.
            </p>
          </div>
        </div>

        <div
          className="section-content sn-mt-40 sn-container sn-mx-auto"
          data-cursor="cursor-opaque"
          data-cursor-text="Drag"
        >
          <Splide
            options={{
              gap: "3rem",
              drag: "free",
              perPage: 4,
              autoHeight: true,
              pagination: false,
              pauseOnHover: true,
              pauseOnFocus: true,
              arrows: false,
              classes: {
                // Add classes for arrows.
                arrows: "splide__arrows sn-slide-arrows",
                arrow: "splide__arrow sn-slide-arrow",
                prev: "splide__arrow--prev sn-slide-prev",
                next: "splide__arrow--next sn-slide-next",

                // Add classes for pagination.
                pagination: "splide__pagination sn-slide-pagination", // container
                page: "splide__pagination__page sn-slide-page", // each button
              },
            }}
          >
            <SplideSlide>
              <div className="section-conten-item sn-inline-block sn-transition-all sn-duration-700 hover:sn-scale-90">
                <img
                  src={inspirationonboardimage}
                  className="apect-[1/1.1] sn-object-cover"
                />
                <div className="summary sn-flex sn-flex-col sn-mt-10 sn-mx-auto sn-space-y-5">
                  <span className="sn-text-lg sn-font-medium sn-text-black sn-flex sn-gap-3 sn-items-center">
                    <Icon icon="ant-design:dribbble-outlined" /> sonibble
                  </span>
                  <span className="sn-text-black sn-text-base sn-text-left">
                    Pomodoro Onboarding Concept
                  </span>
                </div>
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="section-conten-item sn-inline-block sn-transition-all sn-duration-700 hover:sn-scale-90">
                <img
                  src={inspirationweatherimage}
                  className="apect-[1/1.1] sn-object-cover"
                />
                <div className="summary sn-flex sn-flex-col sn-mt-10 sn-mx-auto sn-space-y-5">
                  <span className="sn-text-lg sn-font-medium sn-text-black sn-flex sn-gap-3 sn-items-center">
                    <Icon icon="ant-design:dribbble-outlined" /> sonibble
                  </span>
                  <span className="sn-text-black sn-text-base sn-text-left">
                    Meteo most advance weather app design
                  </span>
                </div>
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="section-conten-item sn-inline-block sn-transition-all sn-duration-700 hover:sn-scale-90">
                <img
                  src={inspirationcardimage}
                  className="apect-[1/1.1] sn-object-cover"
                />
                <div className="summary sn-flex sn-flex-col sn-mt-10 sn-mx-auto sn-space-y-5">
                  <span className="sn-text-lg sn-font-medium sn-text-black sn-flex sn-gap-3 sn-items-center">
                    <Icon icon="ant-design:dribbble-outlined" /> sonibble
                  </span>
                  <span className="sn-text-black sn-text-base sn-text-left">
                    Card design with 3D illustrator
                  </span>
                </div>
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="section-conten-item sn-inline-block sn-transition-all sn-duration-700 hover:sn-scale-90">
                <img
                  src={inspirationotherimage}
                  className="apect-[1/1.1] sn-object-cover"
                />
                <div className="summary sn-flex sn-flex-col sn-mt-10 sn-mx-auto sn-space-y-5">
                  <span className="sn-text-lg sn-font-medium sn-text-black sn-flex sn-gap-3 sn-items-center">
                    <Icon icon="ant-design:dribbble-outlined" /> sonibble
                  </span>
                  <span className="sn-text-black sn-text-base sn-text-left">
                    Pomodoro Screen Layouting
                  </span>
                </div>
              </div>
            </SplideSlide>
          </Splide>
        </div>
      </section>
    </main>
  );
}
