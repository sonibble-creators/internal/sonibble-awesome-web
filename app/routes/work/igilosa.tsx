import igilosaworkimage1 from "~/images/work/igilosa/1.png";
import igilosaworkimage2 from "~/images/work/igilosa/2.png";
import igilosaworkimage3 from "~/images/work/igilosa/3.png";
import igilosaworkimage4 from "~/images/work/igilosa/4.png";
import igilosaworkimage5 from "~/images/work/igilosa/5.png";
import igilosaworkimage6 from "~/images/work/igilosa/6.png";
import igilosaworkimage7 from "~/images/work/igilosa/7.png";
import igilosaworkimage8 from "~/images/work/igilosa/8.png";
import igilosaworkimage9 from "~/images/work/igilosa/9.png";
import igilosaworkimage10 from "~/images/work/igilosa/10.png";
import igilosaworkimage11 from "~/images/work/igilosa/11.png";
import igilosaworkimage12 from "~/images/work/igilosa/12.png";
import { Link, MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @return JSX.Element
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Igilosa Work",
    description: "Grocery now live with modern way and just in your hand",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # IgilosaWorkPage
 *
 * Contain all data and information about igilosa project
 * @return JSX.Element
 */
export default function IgilosaWorkPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HEADING SECTION */}
      <section
        className="section heading-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-32 md:sn-mt-52"
        data-jelly-scroll
      >
        <span
          className="sn-text-lg md:sn-text-xl sn-font-medium sn-text-black"
          data-cursor-text="Work"
        >
          Igilosa
        </span>
        <h2 className="sn-text-3xl md:sn-text-display-2 sn-font-bold sn-mt-11">
          Grocery now live with modern way and just in your hand
        </h2>
      </section>

      {/* IMAGE SECTION */}
      <section
        className="section image-section sn-m-28 md:sn-mt-60"
        data-jelly-scroll
      >
        <img
          src={igilosaworkimage1}
          alt="Sonibble Koso Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-indigo-200"
        />
      </section>

      {/* INTRO SECTION */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-xl md:sn-text-2xl">
            The challenge
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            Everday we always need to eat a healty food, some of grocery app
            provide just the basic feature and look so boring. We have an idea
            to bring the grocery easy to get with just your hand.
          </p>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal sn-mt-10">
            We add more feature and other intuitive touch to make the app become
            more reliable. The design concept come with simple combination but
            more powerful
          </p>

          <div className="sn-flex sn-mt-16 sn-gap-6">
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Download
              </span>
            </Link>
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Source
              </span>
            </Link>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={igilosaworkimage2}
          alt="Sonibble Koso Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-violet-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12">
            Bring all need more simple and fast
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            Combine with many feature and design with very high quality we
            always make the design become more intuitive. Include the friendly
            colors, button, and other component that look seamlessly
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={igilosaworkimage3}
          alt="Sonibble Koso Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-cyan-200"
        />
      </section>

      {/* BRANDING */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-28 md:sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={igilosaworkimage4}
              alt="Sonibble Koso Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Simple shopping bag logo with some masking background make the
              logo clean, and simple
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={igilosaworkimage5}
              alt="Sonibble Koso Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              We make the application suited with their need using Epilogue font
              to become more match
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-80">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={igilosaworkimage6}
              alt="Sonibble Koso Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              We pick illustration from other creator and it’s free. The style
              look matching and it’s help the design more better
            </span>
          </div>
        </div>
      </section>

      {/* DESIGN */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-8/12">
            Get all your grocery need more fast and clean
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            We design the application to enable fast, and managed order. We
            ensure the customer experience more than other element, so we make
            the each element with heart
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={igilosaworkimage7}
          alt="Sonibble Koso Design Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-fuchsia-200"
        />
      </section>

      {/* Feature */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-xl md:sn-text-2xl">
            Take care all of your budget
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            We make a wallet to enable customer manage their budget for grocery.
            Easy way to pay, and send money to other and family.
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={igilosaworkimage8}
          alt="Sonibble Koso Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-indigo-200"
        />
      </section>

      {/* FEATURE */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12">
            How fast if you can do it in one hand
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            We design the application to become more powerful, with high backend
            and services. Allow customer to buy their need using one hand and
            feel fast like a splash. Don’t miss with every feature we make
          </p>
        </div>
      </section>

      {/* FEATURES */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-28 md:sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={igilosaworkimage9}
              alt="Sonibble Koso Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              See the best offer everday and week, get some discount, and free
              grocery item
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={igilosaworkimage10}
              alt="Sonibble Koso Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Take care all about your product, see later, buy and remove just
              like easy to doing
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-96">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={igilosaworkimage11}
              alt="Sonibble Koso Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Get notify when your product comes, and your order in process. All
              detail we will send to you
            </span>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80 sn-mb-44 md:sn-mb-96"
        data-jelly-scroll
      >
        <img
          src={igilosaworkimage12}
          alt="Sonibble Koso Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-green-200"
        />
      </section>
    </main>
  );
}
