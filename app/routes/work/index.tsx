import meteoworkimage from "~/images/work/meteo.png";
import cawisworkimage from "~/images/work/cawis.png";
import drilluworkimage from "~/images/work/drillu.png";
import kosoworkimage from "~/images/work/koso.png";
import igilosaworkimage from "~/images/work/igilosa.png";
import desenoideaimage from "~/images/idea-item-deseno.png";
import devooideaimage from "~/images/idea-item-devoo.png";
import { Link, MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @returns MetaFunction
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Work",
    description: "Explore great and awesome works",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # WorkPage
 *
 * the home page that contain data to show into user when comes
 * @return JSX.Element
 */
export default function WorkPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HERO SECTION */}
      {/* use in every type of page with header and introductions */}
      {/* it will contain some animation like hover effect and masking the text */}
      <section
        className="sn-flex sn-flex-col sn-container sn-mx-auto  sn-pt-20 md:sn-pt-32 sn-pb-32 md:sn-pb-52"
        data-jelly-scroll
        data-cursor="before:sn-bg-purple-500"
      >
        <div className="sn-flex hero-headline-animated sn-flex-col sn-items-center sn-relative sn-overflow-hidden">
          {/* add the shape for masking the text  */}
          {/* will be there, with some feature and shapes rect */}
          <div className="hero-headline-shape sn-bg-black sn-absolute sn-inset-5">
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[50vw] sn-h-[50vw] sn-rounded-full sn-z-30 sn-bg-[#4f2cee] sn-ml-[calc(-50vw/2)] sn-mt-[calc(-50vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[35vw] sn-h-[35vw] sn-rounded-full sn-z-30 sn-bg-[#cf38fd] sn-ml-[calc(-35vw/2)] sn-mt-[calc(-35vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[20vw] sn-h-[20vw] sn-rounded-full sn-z-30 sn-bg-[#fdc838] sn-ml-[calc(-20vw/2)] sn-mt-[calc(-20vw/2)] sn-mix-blend-screen"></div>
          </div>

          {/* the text with some animation comes */}
          <div className="hero-headline-text sn-flex sn-flex-col sn-justify-center sn-mix-blend-screen sn-bg-white sn-w-full">
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              See the best of
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              amazing work around the world
            </span>
          </div>
        </div>
      </section>

      {/* PROJECT SECTION */}
      {/* contain the featured project of work, including the onprogress project */}
      <section
        className="featured-work-section sn-flex sn-mt-40 md:sn-mt-80 sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mb-40 md:sn-mb-80"
        data-jelly-scroll
      >
        {/* fatured works */}
        {/* contain list of feature project, works */}
        <div className="featured-works sn-flex sn-flex-col md:sn-flex-row sn-gap-16">
          {/* left side */}
          <div className="featured-work-collum-left sn-gap-20 sn-flex sn-flex-col md:sn-w-1/2">
            <div className="featured-work-item md:sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="meteo">
                  <img
                    src={meteoworkimage}
                    alt="Sonibble Meteo Work"
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-red-300"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-xl md:sn-text-2xl sn-text-center">
                  Meteo
                </span>
                <p className="sn-text-black sn-font-normal sn-text-lg md:sn-text-xl sn-text-center md:sn-w-4/5 sn-leading-7 md:sn-leading-9">
                  Advance Weather application that include alerts, info, and
                  sport event
                </p>
              </div>
            </div>

            <div className="featured-work-item md:sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="drillu">
                  <img
                    src={drilluworkimage}
                    alt="Sonibble Drillu Work"
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-orange-200"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-xl md:sn-text-2xl sn-text-center">
                  Drillu
                </span>
                <p className="sn-text-black sn-font-normal sn-text-lg md:sn-text-xl sn-text-center md:sn-w-4/5 sn-leading-7 md:sn-leading-9">
                  Best platform to enhance your self to be more valueable
                </p>
              </div>
            </div>

            <div className="featured-work-item md:sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="igilosa">
                  <img
                    src={igilosaworkimage}
                    alt="Sonibble Igilosa Work"
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-green-200"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-xl md:sn-text-2xl sn-text-center">
                  Igilosa
                </span>
                <p className="sn-text-black sn-font-normal sn-text-lg md:sn-text-xl sn-text-center md:sn-w-4/5 sn-leading-7 md:sn-leading-9">
                  Most powerful and featured grocery app based on modern way
                </p>
              </div>
            </div>
          </div>

          {/* right side */}
          <div className="featured-work-collumn-right sn-flex sn-flex-col md:sn-mt-60 md:sn-w-1/2 sn-gap-20">
            <div className="featured-work-item md:sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="cawis">
                  <img
                    src={cawisworkimage}
                    alt="Sonibble Cawis Work"
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-purple-200"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-xl md:sn-text-2xl sn-text-center">
                  Cawis
                </span>
                <p className="sn-text-black sn-font-normal sn-text-lg md:sn-text-xl sn-text-center md:sn-w-4/5 sn-leading-7 md:sn-leading-9">
                  Creative quiz app to handle to boost the creativity in this
                  world
                </p>
              </div>
            </div>

            <div className="featured-work-item md:sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="koso">
                  <img
                    src={kosoworkimage}
                    alt="Sonibble Koso Work"
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-green-200"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-xl md:sn-text-2xl sn-text-center">
                  Koso
                </span>
                <p className="sn-text-black sn-font-normal sn-text-lg md:sn-text-xl sn-text-center md:sn-w-4/5 sn-leading-7 md:sn-leading-9">
                  Simple and powerful task manager app for your productivity
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* IDEA SHOW */}
      {/* contain the recent product to educate others */}
      <section
        className="idea-educate-section sn-flex sn-flex-col sn-mt-40 md:sn-mt-80 sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mb-40 md:sn-mb-96"
        data-jelly-scroll
      >
        <div className="heading-section sn-flex sn-flex-col md:sn-items-end">
          <div
            className="headline-section sn-flex sn-flex-col md:sn-w-2/3"
            data-cursor="cursor-lg"
          >
            <span className="sn-text-3xl md:sn-text-display-2 sn-text-black sn-font-bold">
              Bring Idea
            </span>
            <span className="sn-text-3xl md:sn-text-display-2 sn-text-black sn-font-bold">
              and start journey
            </span>
          </div>
          <div className="headline-description-section sn-flex sn-justify-start sn-mt-10 md:sn-w-2/3">
            <p className="sn-text-black sn-font-normal sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10">
              Sharing is always become first stage. Learn everything about
              technology, design, development, branding, and more.
            </p>
          </div>
        </div>

        <div className="section-content sn-mt-36 sn-flex sn-flex-col md:sn-flex-row sn-justify-end sn-gap-11">
          <div className="section-conten-item sn-flex sn-flex-col sn-justify-center sn-items-center md:sn-max-w-xs">
            <Link to="/product/devoo">
              <img
                src={devooideaimage}
                className="apect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Learn More"
                data-cursor="before:sn-bg-cyan-200"
              />
            </Link>
            <div className="summary sn-flex sn-flex-col sn-justify-center sn-items-center sn-mt-10 md:sn-w-4/5 sn-mx-auto sn-space-y-5">
              <span className="sn-text-xl sn-font-medium sn-text-black">
                Devoo
              </span>
              <span className="sn-text-black sn-text-base sn-text-center">
                Learn development and become super stack developer for free
              </span>
            </div>
          </div>
          <div className="section-conten-item sn-flex sn-flex-col sn-justify-center sn-items-center md:sn-max-w-xs">
            <Link to="/product/deseno">
              <img
                src={desenoideaimage}
                className="apect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Learn More"
                data-cursor="before:sn-bg-pink-200"
              />
            </Link>
            <div className="summary sn-flex sn-flex-col sn-justify-center sn-items-center sn-mt-10 md:sn-w-4/5 sn-mx-auto sn-space-y-5">
              <span className="sn-text-xl sn-font-medium sn-text-black">
                Deseno
              </span>
              <span className="sn-text-black sn-text-base sn-text-center">
                Learn design and become expert designer for free
              </span>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}
