import meteoworkimage1 from "~/images/work/meteo/1.png";
import meteoworkimage2 from "~/images/work/meteo/2.png";
import meteoworkimage3 from "~/images/work/meteo/3.png";
import meteoworkimage4 from "~/images/work/meteo/4.png";
import meteoworkimage5 from "~/images/work/meteo/5.png";
import meteoworkimage6 from "~/images/work/meteo/6.png";
import meteoworkimage7 from "~/images/work/meteo/7.png";
import meteoworkimage8 from "~/images/work/meteo/8.png";
import meteoworkimage9 from "~/images/work/meteo/9.png";
import meteoworkimage10 from "~/images/work/meteo/10.png";
import meteoworkimage11 from "~/images/work/meteo/11.png";
import meteoworkimage12 from "~/images/work/meteo/12.png";
import { Link, MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @return JSX.Element
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Meteo Work",
    description: "Advance weather application that include everyting you need",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # MeteoWorkPage
 *
 * Contain all data and information about meteo project
 * @return JSX.Element
 */
export default function MeteoWorkPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HEADING SECTION */}
      <section
        className="section heading-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-32 md:sn-mt-52"
        data-jelly-scroll
      >
        <span
          className="sn-text-lg md:sn-text-xl sn-font-medium sn-text-black"
          data-cursor-text="Work"
        >
          Meteo
        </span>
        <h2 className="sn-text-3xl md:sn-text-display-2 sn-font-bold sn-mt-11">
          Advance weather application that include everyting you need
        </h2>
      </section>

      {/* IMAGE SECTION */}
      <section
        className="section image-section sn-mt-32 md:sn-mt-60"
        data-jelly-scroll
      >
        <img
          src={meteoworkimage1}
          alt="Sonibble Meteo Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-purple-200"
        />
      </section>

      {/* INTRO SECTION */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-xl md:sn-text-2xl">
            The challenge
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            Weather is an crucial element for most of the people around the
            world. There some activities will be delayed, canceled just caused
            by unknowing the weather conditions. We need to bring some education
            to other people to know the condition out there.
          </p>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal sn-mt-10">
            Meteo come into an creative idea to help smart and creative people
            scheduling their activities without worring about some information
            like event, air, alerts, and sports.
          </p>

          <div className="sn-flex sn-mt-16 sn-gap-6">
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Download
              </span>
            </Link>
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Source
              </span>
            </Link>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5  md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={meteoworkimage2}
          alt="Sonibble Meteo Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-orange-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-5/12">
            Amazing & Simple Branding
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            Try the simple user interface and easy for user to use the
            application without need much effort. Start create an simple logo,
            color picking, and typhography to make teh application user friendly
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={meteoworkimage3}
          alt="Sonibble Meteo Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-purple-200"
        />
      </section>

      {/* BRANDING */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-80 sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={meteoworkimage4}
              alt="Sonibble Meteo Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              The first, was creating an simple logo and easy to understand by
              most of the people
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={meteoworkimage5}
              alt="Sonibble Meteo Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Using Poppins for the typography, the fonts character is friendly,
              strong to branding
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-80">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={meteoworkimage6}
              alt="Sonibble Meteo Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              The important thing is the icon, using community icons that suited
              to the application concept
            </span>
          </div>
        </div>
      </section>

      {/* DESIGN */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-10/12">
            Comes with simple and powerful design
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-10">
            Try to add simple and easy to use design combine with simple colors
            combination will bring the app more ability and focus user will
            increase. We try the best to build application become awesome
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={meteoworkimage7}
          alt="Sonibble Meteo Design Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-purple-200"
        />
      </section>

      {/* Feature */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-xl md:sn-text-2xl">
            Dark Mode Supported
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-8 md:sn-leading-10 sn-font-normal">
            Adding the dark mode into app will help the user change theme by
            their preferences. Dark mode will increase focus user when the night
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={meteoworkimage8}
          alt="Sonibble Meteo Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-indigo-200"
        />
      </section>

      {/* FEATURE */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-6/12">
            More features will helping more
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-8 md:sn-leading-10">
            Base concept weather app in like common, so we decided to add more
            feature to help people manage their time by looking the wather
            condition for a moment
          </p>
        </div>
      </section>

      {/* FEATURES */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={meteoworkimage9}
              alt="Sonibble Meteo Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Astronomy showing the sun and moon rise and set
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={meteoworkimage10}
              alt="Sonibble Meteo Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              See what happen in your location caused by bad weather conditions
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-96">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={meteoworkimage11}
              alt="Sonibble Meteo Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Show you the sport event in your current location base on 3 days
            </span>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80 sn-mb-44 md:sn-mb-96"
        data-jelly-scroll
      >
        <img
          src={meteoworkimage12}
          alt="Sonibble Meteo Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-purple-200"
        />
      </section>
    </main>
  );
}
