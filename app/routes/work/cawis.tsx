import cawisworkimage1 from "~/images/work/cawis/1.png";
import cawisworkimage2 from "~/images/work/cawis/2.png";
import cawisworkimage3 from "~/images/work/cawis/3.png";
import cawisworkimage4 from "~/images/work/cawis/4.png";
import cawisworkimage5 from "~/images/work/cawis/5.png";
import cawisworkimage6 from "~/images/work/cawis/6.png";
import cawisworkimage7 from "~/images/work/cawis/7.png";
import cawisworkimage8 from "~/images/work/cawis/8.png";
import cawisworkimage9 from "~/images/work/cawis/10.png";
import cawisworkimage10 from "~/images/work/cawis/11.png";
import cawisworkimage11 from "~/images/work/cawis/12.png";
import cawisworkimage12 from "~/images/work/cawis/13.png";
import { Link, MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @return JSX.Element
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Cawis Work",
    description: "Creative quiz app with interactive feature for boosting",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # CawisWorkPage
 *
 * Contain all data and information about  project
 * @return JSX.Element
 */
export default function CawisWorkPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HEADING SECTION */}
      <section
        className="section heading-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-28 md:sn-mt-52"
        data-jelly-scroll
      >
        <span
          className="sn-text-lg md:sn-text-xl sn-font-medium sn-text-black"
          data-cursor-text="Work"
        >
          Cawis
        </span>
        <h2 className="sn-text-3xl md:sn-text-display-2 sn-font-bold sn-mt-11">
          Creative quiz app with interactive feature for boosting
        </h2>
      </section>

      {/* IMAGE SECTION */}
      <section
        className="section image-section sn-mt-28 md:sn-mt-60"
        data-jelly-scroll
      >
        <img
          src={cawisworkimage1}
          alt="Sonibble Cawis Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-orange-200"
        />
      </section>

      {/* INTRO SECTION */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-xl md:sn-text-2xl">
            The challenge
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            Quiz app not easy to create, we need to make some gamification and
            other special effect in the application. So the user wil be intent
            use and learning together in our platfrom.
          </p>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal sn-mt-10">
            Sometimes we need to create a good animation, and there are so much
            interactive value, and element using, choosing good colors, fonts is
            also be crucial.
          </p>

          <div className="sn-flex sn-mt-16 sn-gap-6">
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Download
              </span>
            </Link>
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Source
              </span>
            </Link>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={cawisworkimage2}
          alt="Sonibble Cawis Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-orange-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12">
            Comes with new design and concept
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            We try everthing become more efficient without leaving the main
            function. We want to make an app look awesome interactive and good
            for user to using the appp every time they want.
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={cawisworkimage3}
          alt="Sonibble Cawis Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-pink-200"
        />
      </section>

      {/* BRANDING */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-28 md:sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={cawisworkimage4}
              alt="Sonibble Cawis Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              The first, was creating an simple logo and easy to understand by
              most of the people
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={cawisworkimage5}
              alt="Sonibble Cawis Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Using DM Sans for the typography, the fonts character is friendly,
              strong to branding
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-80">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={cawisworkimage6}
              alt="Sonibble Cawis Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Illustration will comes with some new design concept, we try to
              use other resource from other creator
            </span>
          </div>
        </div>
      </section>

      {/* DESIGN */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-10/12">
            We never miss with powerful design and functionality
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            Try to add simple and easy to use design combine with simple colors
            combination will bring the app more ability and focus user will
            increase. We try the best to build application become awesome
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={cawisworkimage7}
          alt="Sonibble Cawis Design Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-fuchia-200"
        />
      </section>

      {/* Feature */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-xl md:sn-text-2xl">
            Dark Mode Supported
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            Adding the dark mode into app will help the user change theme by
            their preferences. Dark mode will increase focus user when the night
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={cawisworkimage8}
          alt="Sonibble Cawis Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-purple-200"
        />
      </section>

      {/* FEATURE */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 sn-w-10/12">
            We Believe in perfection without compromizing
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            All of the feature work well in ther application spec, we alwasy
            care about what we build, try the best and deserve a beautiful
            product around the world
          </p>
        </div>
      </section>

      {/* FEATURES */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-28 md:sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={cawisworkimage9}
              alt="Sonibble Cawis Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Manage all of your selected quiz, update, and delete everytime
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={cawisworkimage10}
              alt="Sonibble Cawis Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              We design the process to be more simpler and easy to use
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-96">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={cawisworkimage11}
              alt="Sonibble Cawis Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Explore the most popular and discover thousand of learning
              resource
            </span>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80 sn-mb-44 md:sn-mb-96"
        data-jelly-scroll
      >
        <img
          src={cawisworkimage12}
          alt="Sonibble Cawis Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-orange-200"
        />
      </section>
    </main>
  );
}
