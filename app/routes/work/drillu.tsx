import drilluworkimage1 from "~/images/work/drillu/1.png";
import drilluworkimage2 from "~/images/work/drillu/2.png";
import drilluworkimage3 from "~/images/work/drillu/3.png";
import drilluworkimage4 from "~/images/work/drillu/4.png";
import drilluworkimage5 from "~/images/work/drillu/5.png";
import drilluworkimage6 from "~/images/work/drillu/6.png";
import drilluworkimage7 from "~/images/work/drillu/7.png";
import drilluworkimage8 from "~/images/work/drillu/8.png";
import drilluworkimage9 from "~/images/work/drillu/9.png";
import drilluworkimage10 from "~/images/work/drillu/10.png";
import drilluworkimage11 from "~/images/work/drillu/11.png";
import drilluworkimage12 from "~/images/work/drillu/12.png";
import { Link, MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @return JSX.Element
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Drillu Work",
    description: "Learning and improve your self without compromizing",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # DrilluWorkPage
 *
 * Contain all data and information about drillu project
 * @return JSX.Element
 */
export default function DrilluWorkPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HEADING SECTION */}
      <section
        className="section heading-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-32 md:sn-mt-52"
        data-jelly-scroll
      >
        <span
          className="sn-text-lg md:sn-text-xl sn-font-medium sn-text-black"
          data-cursor-text="Work"
        >
          Drillu
        </span>
        <h2 className="sn-text-3xl md:sn-text-display-2 sn-font-bold sn-mt-11">
          Learning and improve your self without compromizing
        </h2>
      </section>

      {/* IMAGE SECTION */}
      <section
        className="section image-section sn-mt-28 md:sn-mt-60"
        data-jelly-scroll
      >
        <img
          src={drilluworkimage1}
          alt="Sonibble Drillu Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-purple-200"
        />
      </section>

      {/* INTRO SECTION */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-xl md:sn-text-2xl">
            The challenge
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            Building reading platform is one of the big project, we need to add
            extra stategy when building the infrastructure, inluding create a
            service, and the backend.
          </p>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal sn-mt-10">
            We start with simple design, easy to use and powerful. Including the
            best animation, and effect, of course we never miss with the
            illustration
          </p>

          <div className="sn-flex sn-mt-16 sn-gap-6">
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Launch
              </span>
            </Link>
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Source
              </span>
            </Link>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={drilluworkimage2}
          alt="Sonibble Drillu Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-purple-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12">
            We always set to be more stronger
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            Before we create a initial project, we are doing some research for
            other competitors, what they have, what we need to add and gove to
            people. We think the design concept more important.
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={drilluworkimage3}
          alt="Sonibble Drillu Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-purple-200"
        />
      </section>

      {/* BRANDING */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-28"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={drilluworkimage4}
              alt="Sonibble Drillu Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              The first, was creating an simple logo and easy to understand by
              most of the people
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={drilluworkimage5}
              alt="Sonibble Drillu Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Using DM Sans for the typography, the fonts character is friendly,
              strong to branding
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-80">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={drilluworkimage6}
              alt="Sonibble Drillu Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Illustration will comes with some new design concept, we try to
              use other resource from other creator
            </span>
          </div>
        </div>
      </section>

      {/* DESIGN */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-10/12">
            Dicover and implement the actual solution
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            Combine all perfect feature with idea exploration will make an
            awesome product, we always take care about the collaboration, so we
            can easily enhance the problem and get the big solution.
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={drilluworkimage7}
          alt="Sonibble Drillu Design Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-orange-200"
        />
      </section>

      {/* Feature */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-40 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-xl md:sn-text-2xl">
            Dark Mode Supported
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            Adding the dark mode into app will help the user change theme by
            their preferences. Dark mode will increase focus user when the night
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={drilluworkimage8}
          alt="Sonibble Drillu Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-indigo-200"
        />
      </section>

      {/* FEATURE */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-8/12">
            We design best feature ever and solve problem
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            we make some decision before create some feature, validate, and
            understand how can this solve the problem. Now we create a bundle of
            feature you can work with
          </p>
        </div>
      </section>

      {/* FEATURES */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-28 md:sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={drilluworkimage9}
              alt="Sonibble Drillu Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Explore everthing, see the creator story, post, and event the
              series of your loved content.
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={drilluworkimage10}
              alt="Sonibble Drillu Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Add everthing into your favorit, saved and read later. Never miss
              your loved content.
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-96">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={drilluworkimage11}
              alt="Sonibble Drillu Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Searching for popular tags, and see what’s inside, and suggest
              some tags for your content
            </span>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80 sn-mb-44 md:sn-mb-96"
        data-jelly-scroll
      >
        <img
          src={drilluworkimage12}
          alt="Sonibble Drillu Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-indigo-500"
        />
      </section>
    </main>
  );
}
