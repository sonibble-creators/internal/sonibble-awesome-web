import kosoworkimage1 from "~/images/work/koso/1.png";
import kosoworkimage2 from "~/images/work/koso/2.png";
import kosoworkimage3 from "~/images/work/koso/3.png";
import kosoworkimage4 from "~/images/work/koso/4.png";
import kosoworkimage5 from "~/images/work/koso/5.png";
import kosoworkimage6 from "~/images/work/koso/6.png";
import kosoworkimage7 from "~/images/work/koso/7.png";
import kosoworkimage8 from "~/images/work/koso/8.png";
import kosoworkimage9 from "~/images/work/koso/9.png";
import kosoworkimage10 from "~/images/work/koso/10.png";
import kosoworkimage11 from "~/images/work/koso/11.png";
import kosoworkimage12 from "~/images/work/koso/12.png";
import { Link, MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @return JSX.Element
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Koso Work",
    description: "Bring your life more simple and predictable",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # KosoWorkPage
 *
 * Contain all data and information about koso project
 * @return JSX.Element
 */
export default function KosoWorkPage(): JSX.Element {
  return (
    <main id="view-main">
      {/* HEADING SECTION */}
      <section
        className="section heading-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-28 md:sn-mt-52"
        data-jelly-scroll
      >
        <span
          className="sn-text-lg md:sn-text-xl sn-font-medium sn-text-black"
          data-cursor-text="Work"
        >
          Koso
        </span>
        <h2 className="sn-text-3xl md:sn-text-display-2 sn-font-bold sn-mt-11">
          Bring your life more simple and predictable
        </h2>
      </section>

      {/* IMAGE SECTION */}
      <section
        className="section image-section sn-mt-28 md:sn-mt-60"
        data-jelly-scroll
      >
        <img
          src={kosoworkimage1}
          alt="Sonibble Koso Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-indigo-200"
        />
      </section>

      {/* INTRO SECTION */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-2xl">
            The challenge
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            Most of all task manager app come with complex feature and little
            hard to using. Some people a round the world comes with many task
            but they need to manage all task in simple way
          </p>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal sn-mt-10">
            We decided to make the awesome app with simple feature, give
            notification and others. Design with simple concept and work
            seamlessly
          </p>

          <div className="sn-flex sn-mt-16 sn-gap-6">
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Download
              </span>
            </Link>
            <Link
              to="/work"
              className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
              data-magnetic
            >
              <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
              <span className="sn-relative group-hover:sn-text-white">
                Source
              </span>
            </Link>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={kosoworkimage2}
          alt="Sonibble Koso Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-violet-200"
        />
      </section>

      {/* INTRO */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-5/12">
            More simple more powerful
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            We create a simple product with simple and user friendly design, the
            combination of colors, font, and other element bring the application
            life with simple way
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={kosoworkimage3}
          alt="Sonibble Koso Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-cyan-200"
        />
      </section>

      {/* BRANDING */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-28 md:sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={kosoworkimage4}
              alt="Sonibble Koso Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Simple task logo with single icon inside. We try to make the logo
              as simple as can, but still powerful
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={kosoworkimage5}
              alt="Sonibble Koso Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              We try to use the same as the concept for the font, so we decide
              to pick IBM Plex Sans
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-80">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={kosoworkimage6}
              alt="Sonibble Koso Branding Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              We pick illustration from other creator and it’s free. The style
              look matching and it’s help the design more better
            </span>
          </div>
        </div>
      </section>

      {/* DESIGN */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12">
            Bring everthing into simple manager
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            We research and doing some exploration to see the people problem,
            and pick some solution. Of cource we never miss the concept we
            begin. Always make the concept become simple but powerful yet
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section image-section sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={kosoworkimage7}
          alt="Sonibble Koso Design Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-fuchsia-200"
        />
      </section>

      {/* Feature */}
      <section
        className="section intro-section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-mt-44 md:sn-mt-96 sn-flex sn-flex-col md:sn-flex-row sn-gap-10"
        data-jelly-scroll
      >
        <div className="md:sn-w-5/12">
          <h3 className="sn-font-medium sn-text-black sn-text-2xl">
            Dark Mode Supported
          </h3>
        </div>
        <div className="md:sn-w-7/12">
          <p className="sn-text-black sn-text-xl md:sn-text-2xl sn-leading-9 md:sn-leading-10 sn-font-normal">
            Adding the dark mode into app will help the user change theme by
            their preferences. Dark mode will increase focus user when the night
          </p>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <img
          src={kosoworkimage8}
          alt="Sonibble Koso Work"
          className="sn-w-full sn-transition-all sn-duration-700 hover:sn-scale-110"
          data-cursor="before:sn-bg-indigo-200"
        />
      </section>

      {/* FEATURE */}
      <section
        className="section sn-container sn-mx-auto sn-px-5 md:sn-px-28 sn-flex sn-flex-col sn-mt-40 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col sn-gap-16">
          <h2 className="sn-text-black sn-font-bold sn-text-3xl md:sn-text-display-2 md:sn-w-7/12">
            Simple feature bring yor day easier
          </h2>
          <p className="sn-text-black sn-text-xl md:sn-text-2xl md:sn-w-10/12 sn-leading-9 md:sn-leading-10">
            Every feature we serve always doing validation and testing before
            it’s implement. We believe in feature to make managing task, and
            notify become easier just in the hand.
          </p>
        </div>
      </section>

      {/* FEATURES */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col md:sn-flex-row sn-mt-40 md:sn-mt-80 sn-gap-28 md:sn-gap-10"
        data-jelly-scroll
      >
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={kosoworkimage9}
              alt="Sonibble Koso Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Manage everthing and filter your task in easy way, just click, and
              run timer
            </span>
          </div>
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={kosoworkimage10}
              alt="Sonibble Koso Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              See all the progress in your account, See the recent activity,
              finish, pending, progress and other.
            </span>
          </div>
        </div>
        <div className="sn-flex sn-flex-col md:sn-w-1/2 sn-gap-28 md:sn-mt-96">
          <div className="sn-flex sn-flex-col sn-items-center">
            <img
              src={kosoworkimage11}
              alt="Sonibble Koso Feature Work"
              className="sn-transition-all sn-duration-700 hover:sn-scale-90"
            />
            <span className="sn-text-black sn-text-lg md:sn-text-xl md:sn-w-10/12 sn-text-center sn-mt-14 sn-leading-8">
              Create, edit, and delete task just in one click. Feel the easy way
              to manage task
            </span>
          </div>
        </div>
      </section>

      {/* IMAGE */}
      <section
        className="section sn-px-5 md:sn-px-24 sn-mt-40 md:sn-mt-80 sn-mb-96"
        data-jelly-scroll
      >
        <img
          src={kosoworkimage12}
          alt="Sonibble Koso Work"
          className="sn-w-full"
          data-cursor="before:sn-bg-green-200"
        />
      </section>
    </main>
  );
}
