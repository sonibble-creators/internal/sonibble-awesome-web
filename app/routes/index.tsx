import meteoworkimage from "~/images/work/meteo.png";
import cawisworkimage from "~/images/work/cawis.png";
import drilluworkimage from "~/images/work/drillu.png";
import kosoworkimage from "~/images/work/koso.png";
import desenoideaimage from "~/images/idea-item-deseno.png";
import devooideaimage from "~/images/idea-item-devoo.png";
import inspirationonboardimage from "~/images/inspiration-onboard-item.png";
import inspirationweatherimage from "~/images/inspiration-weather-item.png";
import inspirationcardimage from "~/images/inspiration-card-item.png";
import inspirationotherimage from "~/images/inspiration-other-item.png";
import { Icon } from "@iconify/react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import { Link } from "remix";

/**
 * # HomePage
 *
 * the home page that contain data to show into user when comes
 * @returns JSX.Element
 */
export default function HomePage() {
  return (
    <main id="view-main">
      {/* HERO SECTION */}
      {/* use in every type of page with header and introductions */}
      {/* it will contain some animation like hover effect and masking the text */}
      <section
        className="sn-flex sn-flex-col sn-container sn-mx-auto sn-pt-10 md:sn-pt-32 sn-pb-24 md:sn-pb-52"
        data-jelly-scroll
        data-cursor="before:sn-bg-purple-500"
      >
        <div className="sn-flex hero-headline-animated sn-flex-col sn-items-center sn-relative sn-overflow-hidden">
          {/* add the shape for masking the text  */}
          {/* will be there, with some feature and shapes rect */}
          <div className="hero-headline-shape sn-bg-black sn-absolute sn-inset-5">
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[50vw] sn-h-[50vw] sn-rounded-full sn-z-30 sn-bg-[#4f2cee] sn-ml-[calc(-50vw/2)] sn-mt-[calc(-50vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[35vw] sn-h-[35vw] sn-rounded-full sn-z-30 sn-bg-[#cf38fd] sn-ml-[calc(-35vw/2)] sn-mt-[calc(-35vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[20vw] sn-h-[20vw] sn-rounded-full sn-z-30 sn-bg-[#fdc838] sn-ml-[calc(-20vw/2)] sn-mt-[calc(-20vw/2)] sn-mix-blend-screen"></div>
          </div>

          {/* the text with some animation comes */}
          <div className="hero-headline-text sn-flex sn-flex-col sn-justify-center sn-mix-blend-screen sn-bg-white sn-w-full">
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              Creative creators
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              for your amazing
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              products
            </span>
          </div>
        </div>
      </section>

      {/* PROJECT SECTION */}
      {/* contain the featured project of work, including the onprogress project */}
      <section
        className="featured-work-section sn-flex sn-flex-col sn-mt-20 md:sn-mt-80 sn-container sn-mx-auto sn-px-5 md:sn-px-28"
        data-jelly-scroll
      >
        {/* headline */}
        <div
          className="headline-section sn-flex sn-justify-start sn-flex-col"
          data-cursor="cursor-lg cursor-inverse"
        >
          <span className="sn-text-3xl md:sn-text-display-2 sn-text-black sn-font-bold">
            What we build
          </span>
          <span className="sn-text-3xl md:sn-text-display-2 sn-text-black sn-font-bold">
            for the loved world?
          </span>
        </div>

        {/* description */}
        <div className="headline-description-section sn-flex sn-justify-start sn-mt-10">
          <p className="sn-text-black sn-font-normal sn-text-lg md:sn-text-2xl md:sn-leading-10 sn-max-w-2xl">
            We stand to bring a new solutions for the world by using creativity
            and collaboration to make a helpful product and benefits.
          </p>
        </div>

        {/* fatured works */}
        {/* contain list of feature project, works */}
        <div className="featured-works sn-flex sn-flex-col md:sn-flex-row sn-mt-20 md:sn-mt-52 md:sn-gap-x-16">
          {/* left side */}
          <div className="featured-work-collum-left sn-flex sn-flex-col">
            <div className="featured-work-item sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="/work/meteo">
                  <img
                    src={meteoworkimage}
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-red-300"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-2xl sn-text-center">
                  Meteo
                </span>
                <p className="sn-text-black sn-font-normal md:sn-text-xl sn-text-center md:sn-w-4/5 md:sn-leading-9">
                  Advance Weather application that include alerts, info, and
                  sport event
                </p>
              </div>
            </div>

            <div className="featured-work-item sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="/work/drillu">
                  <img
                    src={drilluworkimage}
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-orange-200"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-2xl sn-text-center">
                  Drillu
                </span>
                <p className="sn-text-black sn-font-normal md:sn-text-xl sn-text-center md:sn-w-4/5 md:sn-leading-9">
                  Best platform to enhance your self to be more valueable
                </p>
              </div>
            </div>
          </div>

          {/* right side */}
          <div className="featured-work-collumn-right sn-flex sn-flex-col md:sn-mt-60">
            <div className="featured-work-item sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="/work/cawis">
                  <img
                    src={cawisworkimage}
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-purple-200"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-2xl sn-text-center">
                  Cawis
                </span>
                <p className="sn-text-black sn-font-normal md:sn-text-xl sn-text-center md:sn-w-4/5 md:sn-leading-9">
                  Creative quiz app to handle to boost the creativity in this
                  world
                </p>
              </div>
            </div>

            <div className="featured-work-item sn-mt-20 sn-w-full sn-flex sn-flex-col">
              <div className="featured-image">
                <Link to="/work/koso">
                  <img
                    src={kosoworkimage}
                    className="sn-aspect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                    data-cursor-text="View More"
                    data-cursor="before:sn-bg-green-200"
                  />
                </Link>
              </div>
              <div className="featured-summary sn-flex sn-flex-col sn-justify-center sn-mt-14 sn-items-center sn-space-y-7">
                <span className="sn-text-black sn-font-medium sn-text-2xl sn-text-center">
                  Koso
                </span>
                <p className="sn-text-black sn-font-normal md:sn-text-xl sn-text-center md:sn-w-4/5 md:sn-leading-9">
                  Simple and powerful task manager app for your productivity
                </p>
              </div>
            </div>
          </div>
        </div>

        {/* actions */}
        <div className="sn-mt-52 sn-flex sn-flex-row sn-justify-center">
          <Link
            to="/work"
            className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
            data-magnetic
          >
            <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
            <span className="sn-relative group-hover:sn-text-white">
              See More Work
            </span>
          </Link>
        </div>
      </section>

      {/* IDEA SHOW */}
      {/* contain the recent product to educate others */}
      <section
        className="idea-educate-section sn-flex sn-flex-col sn-mt-60 md:sn-mt-80 sn-container sn-mx-auto sn-px-5 md:sn-px-28"
        data-jelly-scroll
      >
        <div className="heading-section sn-flex sn-flex-col md:sn-items-end">
          <div
            className="headline-section sn-flex sn-flex-col md:sn-w-2/3"
            data-cursor="cursor-lg"
          >
            <span className="sn-text-3xl md:sn-text-display-2 sn-text-black sn-font-bold">
              Bring Idea
            </span>
            <span className="sn-text-3xl md:sn-text-display-2 sn-text-black sn-font-bold">
              and start journey
            </span>
          </div>
          <div className="headline-description-section sn-flex sn-justify-start sn-mt-10 md:sn-w-2/3">
            <p className="sn-text-black sn-font-normal sn-text-lg md:sn-text-2xl md:sn-leading-10">
              Sharing is always become first stage. Learn everything about
              technology, design, development, branding, and more.
            </p>
          </div>
        </div>

        <div className="section-content sn-mt-36 sn-flex sn-flex-col sn-gap-10 md:sn-flex-row sn-justify-end md:sn-gap-x-11">
          <div className="section-conten-item sn-flex sn-flex-col sn-justify-center sn-items-center sn-max-w-xs">
            <Link to="">
              <img
                src={devooideaimage}
                className="apect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Coming Soon"
                data-cursor="before:sn-bg-cyan-200"
              />
            </Link>
            <div className="summary sn-flex sn-flex-col sn-justify-center sn-items-center sn-mt-10 md:sn-w-4/5 sn-mx-auto sn-space-y-5">
              <span className="sn-text-xl sn-font-medium sn-text-black">
                Devoo
              </span>
              <span className="sn-text-black sn-text-base sn-text-center">
                Learn development and become super stack developer for free
              </span>
            </div>
          </div>
          <div className="section-conten-item sn-flex sn-flex-col sn-justify-center sn-items-center sn-max-w-xs">
            <Link to="">
              <img
                src={desenoideaimage}
                className="apect-[1/1.3] sn-object-cover sn-transition-all sn-duration-700 hover:sn-scale-90"
                data-cursor-text="Coming Soon"
                data-cursor="before:sn-bg-pink-200"
              />
            </Link>
            <div className="summary sn-flex sn-flex-col sn-justify-center sn-items-center sn-mt-10 md:sn-w-4/5 sn-mx-auto sn-space-y-5">
              <span className="sn-text-xl sn-font-medium sn-text-black">
                Deseno
              </span>
              <span className="sn-text-black sn-text-base sn-text-center">
                Learn design and become expert designer for free
              </span>
            </div>
          </div>
        </div>
      </section>

      {/* INSPIRATION SECTION */}
      {/* contain inspiration post and other resource */}
      <section
        className="idea-educate-section sn-flex sn-flex-col sn-mt-60 md:sn-mt-80 sn-mb-40 md:sn-mb-80"
        data-jelly-scroll
      >
        <div className="heading-section sn-flex sn-flex-col sn-container sn-mx-auto sn-px-5 md:sn-px-28">
          <div
            className="headline-section sn-flex sn-flex-col md:sn-w-2/3"
            data-cursor="cursor-lg"
          >
            <span className="sn-text-3xl md:sn-text-display-2 sn-text-black sn-font-bold">
              New day
            </span>
            <span className="sn-text-3xl md:sn-text-display-2 sn-text-black sn-font-bold">
              new inpirations
            </span>
          </div>
          <div className="headline-description-section sn-flex sn-justify-start sn-mt-10 md:sn-w-2/3">
            <p className="sn-text-black sn-font-normal sn-text-lg md:sn-text-2xl">
              Update with some inspiration. Let you mind blow up and bring
              creative idea.
            </p>
          </div>
        </div>

        <div
          className="section-content sn-mt-40 sn-container sn-mx-auto"
          data-cursor="cursor-opaque"
          data-cursor-text="Drag"
        >
          <Splide
            options={{
              gap: "3rem",
              drag: "free",
              perPage: 4,
              autoHeight: true,
              pagination: false,
              pauseOnHover: true,
              pauseOnFocus: true,
              arrows: false,
              classes: {
                // Add classes for arrows.
                arrows: "splide__arrows sn-slide-arrows",
                arrow: "splide__arrow sn-slide-arrow",
                prev: "splide__arrow--prev sn-slide-prev",
                next: "splide__arrow--next sn-slide-next",

                // Add classes for pagination.
                pagination: "splide__pagination sn-slide-pagination", // container
                page: "splide__pagination__page sn-slide-page", // each button
              },
            }}
          >
            <SplideSlide>
              <div className="section-conten-item sn-inline-block sn-transition-all sn-duration-700 hover:sn-scale-90">
                <img
                  src={inspirationonboardimage}
                  className="apect-[1/1.1] sn-object-cover"
                />
                <div className="summary sn-flex sn-flex-col sn-mt-10 sn-mx-auto sn-space-y-5">
                  <span className="sn-text-lg sn-font-medium sn-text-black sn-flex sn-gap-3 sn-items-center">
                    <Icon icon="ant-design:dribbble-outlined" /> sonibble
                  </span>
                  <span className="sn-text-black sn-text-base sn-text-left">
                    Pomodoro Onboarding Concept
                  </span>
                </div>
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="section-conten-item sn-inline-block sn-transition-all sn-duration-700 hover:sn-scale-90">
                <img
                  src={inspirationweatherimage}
                  className="apect-[1/1.1] sn-object-cover"
                />
                <div className="summary sn-flex sn-flex-col sn-mt-10 sn-mx-auto sn-space-y-5">
                  <span className="sn-text-lg sn-font-medium sn-text-black sn-flex sn-gap-3 sn-items-center">
                    <Icon icon="ant-design:dribbble-outlined" /> sonibble
                  </span>
                  <span className="sn-text-black sn-text-base sn-text-left">
                    Meteo most advance weather app design
                  </span>
                </div>
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="section-conten-item sn-inline-block sn-transition-all sn-duration-700 hover:sn-scale-90">
                <img
                  src={inspirationcardimage}
                  className="apect-[1/1.1] sn-object-cover"
                />
                <div className="summary sn-flex sn-flex-col sn-mt-10 sn-mx-auto sn-space-y-5">
                  <span className="sn-text-lg sn-font-medium sn-text-black sn-flex sn-gap-3 sn-items-center">
                    <Icon icon="ant-design:dribbble-outlined" /> sonibble
                  </span>
                  <span className="sn-text-black sn-text-base sn-text-left">
                    Card design with 3D illustrator
                  </span>
                </div>
              </div>
            </SplideSlide>
            <SplideSlide>
              <div className="section-conten-item sn-inline-block sn-transition-all sn-duration-700 hover:sn-scale-90">
                <img
                  src={inspirationotherimage}
                  className="apect-[1/1.1] sn-object-cover"
                />
                <div className="summary sn-flex sn-flex-col sn-mt-10 sn-mx-auto sn-space-y-5">
                  <span className="sn-text-lg sn-font-medium sn-text-black sn-flex sn-gap-3 sn-items-center">
                    <Icon icon="ant-design:dribbble-outlined" /> sonibble
                  </span>
                  <span className="sn-text-black sn-text-base sn-text-left">
                    Pomodoro Screen Layouting
                  </span>
                </div>
              </div>
            </SplideSlide>
          </Splide>
        </div>
      </section>
    </main>
  );
}
