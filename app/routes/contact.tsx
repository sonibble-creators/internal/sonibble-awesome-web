import { useState } from "react";
import { Form, MetaFunction } from "remix";

/**
 * meta
 *
 * Allow to add meta tags for SEO
 * @returns MetaFunction
 */
export const meta: MetaFunction = () => {
  return {
    title: "Sonibble - Contact",
    description: "Stay close, and ask anything anytime for use sonibble",
    keyword:
      "Sonibble, Idea, Freelancer, Creative Agency, Product Designer, Developer, Designer",
    "theme-color": "#000",
  };
};

/**
 * # ContactPage
 * Show the contact info,
 * send message, and other action for users
 * @return JSX.Element
 */
export default function ContactPage(): JSX.Element {
  // define the static variable
  const interestedLists: string[] = [
    "Development",
    "Design",
    "Back End",
    "Product Design",
    "Mobile",
    "Apps From Scratch",
    "API",
    "Web",
  ];

  // define the variable
  // use to add and store all of interest data list
  // enable you to add and using state of added lists
  const [selectedInterestedList, setselectedInterestedList] = useState<
    string[]
  >([]);

  // when the user the interested
  // will trigger this function
  // add the interest into selected list if not yet, and delete if already
  // exist
  const onInterestedClicked: Function = (interested: string) => {
    var lists = [...selectedInterestedList];

    // we need to check the data
    // already exist, or not
    const isSelected = lists.includes(interested);
    if (isSelected) {
      // we going to remove the list in selected
      // and store again
      var index = lists.indexOf(interested);
      if (index > -1) {
        // start remove
        lists.splice(index, 1);
      }

      // now we need to store the data into variabless
      setselectedInterestedList(lists);
    } else {
      // we going to add into selected data
      var lists = [...selectedInterestedList];
      lists.push(interested);

      // store the data
      setselectedInterestedList(lists);
    }
  };

  return (
    <main id="view-main">
      {/* HERO SECTION */}
      {/* use in every type of page with header and introductions */}
      {/* it will contain some animation like hover effect and masking the text */}
      <section
        className="sn-flex sn-flex-col sn-container sn-mx-auto sn-pt-20 md:sn-pt-32 sn-pb-32 md:sn-pb-52"
        data-jelly-scroll
        data-cursor="before:sn-bg-purple-500"
      >
        <div className="sn-flex hero-headline-animated sn-flex-col sn-items-center sn-relative sn-overflow-hidden">
          {/* add the shape for masking the text  */}
          {/* will be there, with some feature and shapes rect */}
          <div className="hero-headline-shape sn-bg-black sn-absolute sn-inset-5">
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[50vw] sn-h-[50vw] sn-rounded-full sn-z-30 sn-bg-[#4f2cee] sn-ml-[calc(-50vw/2)] sn-mt-[calc(-50vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[35vw] sn-h-[35vw] sn-rounded-full sn-z-30 sn-bg-[#cf38fd] sn-ml-[calc(-35vw/2)] sn-mt-[calc(-35vw/2)] sn-mix-blend-screen"></div>
            <div className="hero-headline-shape-item shape-circle sn-left-0 sn-top-0 sn-absolute sn-w-[20vw] sn-h-[20vw] sn-rounded-full sn-z-30 sn-bg-[#fdc838] sn-ml-[calc(-20vw/2)] sn-mt-[calc(-20vw/2)] sn-mix-blend-screen"></div>
          </div>

          {/* the text with some animation comes */}
          <div className="hero-headline-text sn-flex sn-flex-col sn-justify-center sn-mix-blend-screen sn-bg-white sn-w-full">
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              Hey, tell us all
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              about the things
            </span>
            <span className="sn-text-4xl md:sn-text-display-1 sn-font-bold sn-text-center">
              and anythings
            </span>
          </div>
        </div>
      </section>

      {/* CONTACT FORM */}
      <section
        className="section sn-container sn-px-5 md:sn-px-28 sn-mx-auto sn-flex sn-flex-col sn-mb-40 md:sn-mb-80 sn-mt-20 md:sn-mt-80"
        data-jelly-scroll
      >
        <div className="flex">
          <h3 className="sn-text-gray-400 sn-text-2xl md:sn-text-4xl sn-font-medium">
            Interested in ....
          </h3>
        </div>
        <div className="sn-flex sn-flex-wrap md:sn-w-8/12 sn-gap-5 md:sn-gap-6 sn-mt-14">
          {interestedLists.map((interested, index) => {
            return (
              <button
                className={
                  selectedInterestedList.includes(interested)
                    ? "sn-text-base sn-font-medium sn-text-white sn-bg-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                    : "sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
                }
                data-magnetic
                key={index}
                onClick={(e) => onInterestedClicked(interested)}
              >
                <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
                <span className="sn-relative group-hover:sn-text-white">
                  {interested}
                </span>
              </button>
            );
          })}
        </div>

        <Form className="sn-mt-20 sn-flex sn-flex-col sn-gap-10">
          <input
            type="text"
            className="sn-outline-none sn-ring-0 sn-border-b sn-border-b-gray-200 sn-w-full placeholder:sn-text-3xl placeholder:sn-text-gray-400 sn-px-6 sn-py-6 sn-text-xl sn-text-black sn-font-medium focus:sn-border-b-2 focus:sn-border-black sn-transition-all sn-duration-700"
            placeholder="Your name"
          />
          <input
            type="email"
            className="sn-outline-none sn-ring-0 sn-border-b sn-border-b-gray-200 sn-w-full placeholder:sn-text-3xl placeholder:sn-text-gray-400 sn-px-6 sn-py-6 sn-text-xl sn-text-black sn-font-medium focus:sn-border-b-2 focus:sn-border-black sn-transition-all sn-duration-700"
            placeholder="Your email address"
          />
          <textarea
            className="sn-outline-none sn-ring-0 sn-border-b sn-border-b-gray-200 sn-w-full placeholder:sn-text-3xl placeholder:sn-text-gray-400 sn-px-6 sn-py-6 sn-text-xl sn-text-black sn-font-medium focus:sn-border-b-2 focus:sn-border-black sn-transition-all sn-duration-700"
            placeholder="Your Message"
          ></textarea>
        </Form>

        <div className="flex sn-mt-40">
          <button
            className="sn-text-base sn-font-medium sn-text-black sn-px-8 sn-py-5 sn-rounded-full sn-border sn-border-light-grey sn-relative sn-overflow-hidden sn-group"
            data-magnetic
          >
            <span className="sn-absolute sn-h-full sn-inset-0 sn-w-0 sn-bg-black sn-rounded-full sn-transition-all sn-duration-700 group-hover:sn-w-full"></span>
            <span className="sn-relative group-hover:sn-text-white">
              Send Message
            </span>
          </button>
        </div>
      </section>
    </main>
  );
}
